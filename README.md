# README #

### Contacts ###

* [http://unknowndev.ru/](Link URL) (Alexandr Milevskiy)

### What is this repository for? ###

* Quick summary
* Version 0.0.1

### Structure ###

1. ### Controllers:
* ru.unknowndev.smarthome.controllers.user - Profile, Login and Registration
* ru.unknowndev.smarthome.controllers.device - Add, Edit and Remove Device
* ru.unknowndev.smarthome.controllers.main - Main menu

1. ### Models:
* ru.unknowndev.smarthome.models.* - All entities db

1. ### Services:
* ru.unknowndev.smarthome.services.RequestHandler - for all requests
* ru.unknowndev.smarthome.services.Settings - settings (set/get url of database and arduino)
* ru.unknowndev.smarthome.services.UsersManager - add, authorization of users
* ru.unknowndev.smarthome.services.DevicesManager - add, update, remove, get of devices

1. ### Views:
* res.layout.* - All templates for views

1. ### Sketch of Arduino:
* sketch_arduino/server/server.ino - Server for arduino

1. ### Web for requests with db:
* web/*.php - get, select, delete, update

1. ### Database backup for web:
* web/smarthome_db.sql - backup