-- phpMyAdmin SQL Dump
-- version 4.6.5.2
-- https://www.phpmyadmin.net/
--
-- Хост: 192.168.1.145:3306
-- Время создания: Апр 02 2017 г., 19:24
-- Версия сервера: 5.6.34
-- Версия PHP: 7.0.14

SET SQL_MODE = "NO_AUTO_VALUE_ON_ZERO";
SET time_zone = "+00:00";


/*!40101 SET @OLD_CHARACTER_SET_CLIENT=@@CHARACTER_SET_CLIENT */;
/*!40101 SET @OLD_CHARACTER_SET_RESULTS=@@CHARACTER_SET_RESULTS */;
/*!40101 SET @OLD_COLLATION_CONNECTION=@@COLLATION_CONNECTION */;
/*!40101 SET NAMES utf8mb4 */;

--
-- База данных: `smarthome_db`
--

-- --------------------------------------------------------

--
-- Структура таблицы `access_levels`
--

CREATE TABLE `access_levels` (
  `id_access_level` int(11) NOT NULL,
  `name` varchar(100) NOT NULL,
  `description` text
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

--
-- Дамп данных таблицы `access_levels`
--

INSERT INTO `access_levels` (`id_access_level`, `name`, `description`) VALUES
(1, 'Администраторы', 'Полный доступ к дому, включая секретные функции'),
(2, 'Пользователи', 'Имеет доступ к функциям управления домом');

-- --------------------------------------------------------

--
-- Структура таблицы `devices`
--

CREATE TABLE `devices` (
  `id_device` int(11) NOT NULL,
  `name` varchar(100) NOT NULL,
  `description` text,
  `status` varchar(100) NOT NULL,
  `pin` int(11) NOT NULL,
  `value` float NOT NULL,
  `id_device_type` int(11) NOT NULL,
  `id_module` int(11) NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

--
-- Дамп данных таблицы `devices`
--

INSERT INTO `devices` (`id_device`, `name`, `description`, `status`, `pin`, `value`, `id_device_type`, `id_module`) VALUES
(2, 'Светильник в зале', 'Который у окна', 'Работает', 7, 0, 1, 2),
(12, 'Electric cooler', 'Cooler', 'Work', 10, 1, 1, 4);

-- --------------------------------------------------------

--
-- Структура таблицы `devices_type`
--

CREATE TABLE `devices_type` (
  `id_device_type` int(11) NOT NULL,
  `name` varchar(100) NOT NULL,
  `description` text
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

--
-- Дамп данных таблицы `devices_type`
--

INSERT INTO `devices_type` (`id_device_type`, `name`, `description`) VALUES
(1, 'Устройства', 'Техника, приборы и т.д'),
(2, 'Датчики', 'Различные датчики движения, температуры и т.д');

-- --------------------------------------------------------

--
-- Структура таблицы `homes`
--

CREATE TABLE `homes` (
  `id_home` int(11) NOT NULL,
  `access_key` varchar(100) NOT NULL,
  `name` varchar(100) NOT NULL,
  `status` varchar(100) NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

--
-- Дамп данных таблицы `homes`
--

INSERT INTO `homes` (`id_home`, `access_key`, `name`, `status`) VALUES
(1, '5558', 'Мой дом', 'Работает');

-- --------------------------------------------------------

--
-- Структура таблицы `modules`
--

CREATE TABLE `modules` (
  `id_module` int(11) NOT NULL,
  `name` varchar(100) NOT NULL,
  `description` text,
  `status` varchar(100) NOT NULL,
  `id_sub_module` int(11) DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

--
-- Дамп данных таблицы `modules`
--

INSERT INTO `modules` (`id_module`, `name`, `description`, `status`, `id_sub_module`) VALUES
(2, 'Управление светом', 'Управляет светом в доме', 'Включен', 0),
(3, 'Климат контроль', 'Отвечает за вентиляцию, кондиционирование и отопление', 'Выключено', NULL),
(4, 'Электроснабжение', 'Управляет электроэнергией', 'Выключено', NULL),
(5, 'Безопасность', 'Управляет безопасностью дома', 'Выключено', NULL),
(6, 'Система развлечений', 'Управляет развлекательными устройствами дома', 'Выключено', NULL);

-- --------------------------------------------------------

--
-- Структура таблицы `operations`
--

CREATE TABLE `operations` (
  `id_operation` int(11) NOT NULL,
  `name` varchar(100) NOT NULL,
  `description` text,
  `date` datetime NOT NULL,
  `id_module` int(11) DEFAULT NULL,
  `id_device` int(11) DEFAULT NULL,
  `id_user` int(11) DEFAULT NULL,
  `id_home` int(11) NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

--
-- Дамп данных таблицы `operations`
--

INSERT INTO `operations` (`id_operation`, `name`, `description`, `date`, `id_module`, `id_device`, `id_user`, `id_home`) VALUES
(37, 'Александр авторизовался', '', '2017-04-02 12:36:43', -1, -1, 1, -1),
(38, 'Модуль Управление светом: Включен', '', '2017-04-02 12:36:56', 2, -1, 1, -1),
(39, 'Устройство Свет в прихожей было удалено', '', '2017-04-02 12:37:21', 2, 5, 1, -1),
(40, 'Александр авторизовался', '', '2017-04-02 13:46:26', -1, -1, 1, -1),
(41, 'Александр авторизовался', '', '2017-04-02 13:47:42', -1, -1, 1, -1),
(42, 'Александр авторизовался', '', '2017-04-02 14:03:59', -1, -1, 1, -1),
(43, 'Александр авторизовался', '', '2017-04-02 14:05:57', -1, -1, 1, -1),
(44, 'Александр авторизовался', '', '2017-04-02 14:08:14', -1, -1, 1, -1),
(45, 'Александр авторизовался', '', '2017-04-02 14:09:43', -1, -1, 1, -1),
(46, 'Александр авторизовался', '', '2017-04-02 14:11:49', -1, -1, 1, -1),
(47, 'Александр авторизовался', '', '2017-04-02 14:31:28', -1, -1, 1, -1),
(48, 'Александр авторизовался', '', '2017-04-02 14:42:00', -1, -1, 1, -1),
(49, 'Александр авторизовался', '', '2017-04-02 14:44:50', -1, -1, 1, -1),
(50, 'Александр авторизовался', '', '2017-04-02 14:45:43', -1, -1, 1, -1),
(51, 'Александр авторизовался', '', '2017-04-02 14:47:25', -1, -1, 1, -1),
(52, 'Александр авторизовался', '', '2017-04-02 14:48:28', -1, -1, 1, -1),
(53, 'Александр авторизовался', '', '2017-04-02 14:52:12', -1, -1, 1, -1),
(54, 'Александр авторизовался', '', '2017-04-02 14:52:46', -1, -1, 1, -1),
(55, 'Александр авторизовался', '', '2017-04-02 14:53:20', -1, -1, 1, -1),
(56, 'Александр авторизовался', '', '2017-04-02 14:54:06', -1, -1, 1, -1),
(57, 'Александр авторизовался', '', '2017-04-02 14:56:09', -1, -1, 1, -1),
(58, 'Александр авторизовался', '', '2017-04-02 14:57:28', -1, -1, 1, -1),
(59, 'Александр авторизовался', '', '2017-04-02 15:00:32', -1, -1, 1, -1),
(60, 'Александр авторизовался', '', '2017-04-02 15:02:17', -1, -1, 1, -1),
(61, 'Александр авторизовался', '', '2017-04-02 15:02:59', -1, -1, 1, -1),
(62, 'Александр авторизовался', '', '2017-04-02 15:03:52', -1, -1, 1, -1),
(63, 'Александр авторизовался', '', '2017-04-02 15:04:55', -1, -1, 1, -1),
(64, 'Александр авторизовался', '', '2017-04-02 15:07:50', -1, -1, 1, -1),
(65, 'Александр авторизовался', '', '2017-04-02 15:09:29', -1, -1, 1, -1),
(66, 'Александр авторизовался', '', '2017-04-02 15:10:49', -1, -1, 1, -1),
(67, 'Александр авторизовался', '', '2017-04-02 15:16:34', -1, -1, 1, -1),
(68, 'Александр авторизовался', '', '2017-04-02 15:19:12', -1, -1, 1, -1),
(69, 'Александр авторизовался', '', '2017-04-02 15:21:37', -1, -1, 1, -1),
(70, 'Александр авторизовался', '', '2017-04-02 15:27:00', -1, -1, 1, -1),
(71, 'Александр авторизовался', '', '2017-04-02 15:32:28', -1, -1, 1, -1),
(72, 'Александр авторизовался', '', '2017-04-02 15:33:23', -1, -1, 1, -1),
(73, 'Александр авторизовался', '', '2017-04-02 15:33:54', -1, -1, 1, -1),
(74, 'Александр авторизовался', '', '2017-04-02 15:35:33', -1, -1, 1, -1),
(75, 'Александр авторизовался', '', '2017-04-02 15:36:26', -1, -1, 1, -1),
(76, 'Александр авторизовался', '', '2017-04-02 15:37:26', -1, -1, 1, -1),
(77, 'Александр авторизовался', '', '2017-04-02 15:39:21', -1, -1, 1, -1),
(78, 'Александр авторизовался', '', '2017-04-02 15:39:51', -1, -1, 1, -1),
(79, 'Александр авторизовался', '', '2017-04-02 15:40:16', -1, -1, 1, -1),
(80, 'Александр авторизовался', '', '2017-04-02 15:41:25', -1, -1, 1, -1),
(81, 'Александр авторизовался', '', '2017-04-02 15:41:51', -1, -1, 1, -1),
(82, 'Александр авторизовался', '', '2017-04-02 15:43:45', -1, -1, 1, -1),
(83, 'Александр авторизовался', '', '2017-04-02 15:46:54', -1, -1, 1, -1),
(84, 'Александр авторизовался', '', '2017-04-02 15:47:20', -1, -1, 1, -1),
(85, 'Александр авторизовался', '', '2017-04-02 15:49:33', -1, -1, 1, -1),
(86, 'Александр авторизовался', '', '2017-04-02 15:51:10', -1, -1, 1, -1),
(87, 'Александр авторизовался', '', '2017-04-02 15:51:38', -1, -1, 1, -1),
(88, 'Александр авторизовался', '', '2017-04-02 15:54:33', -1, -1, 1, -1),
(89, 'Александр авторизовался', '', '2017-04-02 15:55:41', -1, -1, 1, -1),
(90, 'Александр авторизовался', '', '2017-04-02 15:57:18', -1, -1, 1, -1),
(91, 'Александр авторизовался', '', '2017-04-02 15:58:43', -1, -1, 1, -1),
(92, 'Александр авторизовался', '', '2017-04-02 16:15:36', -1, -1, 1, -1),
(93, 'Александр авторизовался', '', '2017-04-02 16:17:34', -1, -1, 1, -1);

-- --------------------------------------------------------

--
-- Структура таблицы `sub_modules`
--

CREATE TABLE `sub_modules` (
  `id_sub_module` int(11) NOT NULL,
  `name` varchar(100) NOT NULL,
  `description` text
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

-- --------------------------------------------------------

--
-- Структура таблицы `users`
--

CREATE TABLE `users` (
  `id_user` int(11) NOT NULL,
  `email` varchar(50) NOT NULL,
  `password` varchar(20) NOT NULL,
  `surname` varchar(20) DEFAULT NULL,
  `name` varchar(20) DEFAULT NULL,
  `patronymic` varchar(20) DEFAULT NULL,
  `birthday` date DEFAULT NULL,
  `gender` varchar(7) DEFAULT NULL,
  `phone` int(20) DEFAULT NULL,
  `id_access_level` int(11) NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

--
-- Дамп данных таблицы `users`
--

INSERT INTO `users` (`id_user`, `email`, `password`, `surname`, `name`, `patronymic`, `birthday`, `gender`, `phone`, `id_access_level`) VALUES
(1, 'admin@smart.home', 'smarthome01', 'Милевский', 'Александр', 'Олегович', '1995-03-08', 'Мужской', 1, 1),
(2, 'testik@testik.testik', 'test', 'Testik', 'Testik', 'Testik', '1995-08-03', 'Мужской', 2147483647, 2);

--
-- Индексы сохранённых таблиц
--

--
-- Индексы таблицы `access_levels`
--
ALTER TABLE `access_levels`
  ADD PRIMARY KEY (`id_access_level`);

--
-- Индексы таблицы `devices`
--
ALTER TABLE `devices`
  ADD PRIMARY KEY (`id_device`);

--
-- Индексы таблицы `devices_type`
--
ALTER TABLE `devices_type`
  ADD PRIMARY KEY (`id_device_type`);

--
-- Индексы таблицы `homes`
--
ALTER TABLE `homes`
  ADD PRIMARY KEY (`id_home`);

--
-- Индексы таблицы `modules`
--
ALTER TABLE `modules`
  ADD PRIMARY KEY (`id_module`);

--
-- Индексы таблицы `operations`
--
ALTER TABLE `operations`
  ADD PRIMARY KEY (`id_operation`);

--
-- Индексы таблицы `sub_modules`
--
ALTER TABLE `sub_modules`
  ADD PRIMARY KEY (`id_sub_module`);

--
-- Индексы таблицы `users`
--
ALTER TABLE `users`
  ADD PRIMARY KEY (`id_user`);

--
-- AUTO_INCREMENT для сохранённых таблиц
--

--
-- AUTO_INCREMENT для таблицы `access_levels`
--
ALTER TABLE `access_levels`
  MODIFY `id_access_level` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=3;
--
-- AUTO_INCREMENT для таблицы `devices`
--
ALTER TABLE `devices`
  MODIFY `id_device` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=13;
--
-- AUTO_INCREMENT для таблицы `devices_type`
--
ALTER TABLE `devices_type`
  MODIFY `id_device_type` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=3;
--
-- AUTO_INCREMENT для таблицы `homes`
--
ALTER TABLE `homes`
  MODIFY `id_home` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=2;
--
-- AUTO_INCREMENT для таблицы `modules`
--
ALTER TABLE `modules`
  MODIFY `id_module` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=7;
--
-- AUTO_INCREMENT для таблицы `operations`
--
ALTER TABLE `operations`
  MODIFY `id_operation` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=94;
--
-- AUTO_INCREMENT для таблицы `sub_modules`
--
ALTER TABLE `sub_modules`
  MODIFY `id_sub_module` int(11) NOT NULL AUTO_INCREMENT;
--
-- AUTO_INCREMENT для таблицы `users`
--
ALTER TABLE `users`
  MODIFY `id_user` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=3;
/*!40101 SET CHARACTER_SET_CLIENT=@OLD_CHARACTER_SET_CLIENT */;
/*!40101 SET CHARACTER_SET_RESULTS=@OLD_CHARACTER_SET_RESULTS */;
/*!40101 SET COLLATION_CONNECTION=@OLD_COLLATION_CONNECTION */;
