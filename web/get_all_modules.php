<?php

//Importing database
require_once('config.php');

//Creating sql query
$sql = "SELECT * FROM modules";

//getting result
$r = mysqli_query($con, $sql);

//creating a blank array
$result = array();

//looping through all the records fetched
while ($row = mysqli_fetch_array($r)) {
    //Pushing name and id in the blank array created
    array_push($result, array(
        "id_module" => $row['id_module'],
        "name" => $row['name'],
        "description" => $row['description'],
        "status" => $row['status'],
        "id_sub_module" => $row['id_sub_module']
    ));
}

//Displaying the array in json format
echo json_encode(array('result_modules' => $result));

mysqli_close($con);
?>