<?php

//Importing database
require_once('config.php');

$dateFrom = $_POST['datefrom'];
$dateTo = $_POST['dateto'];

//Creating sql query
$sql = "SELECT * FROM operations WHERE date > '$dateFrom' AND date < '$dateTo'";

//getting result
$r = mysqli_query($con, $sql);

//creating a blank array
$result = array();

//looping through all the records fetched
while ($row = mysqli_fetch_array($r)) {
    //Pushing name and id in the blank array created
    array_push($result, array(
        "id_operation" => $row['id_operation'],
        "name" => $row['name'],
        "description" => $row['description'],
        "date" => $row['date'],
        "id_module" => $row['id_module'],
        "id_user" => $row['id_user'],
        "id_device" => $row['id_device'],
        "id_home" => $row['id_home']
    ));
}

//Displaying the array in json format
echo json_encode(array('result_operations' => $result));

mysqli_close($con);
?>