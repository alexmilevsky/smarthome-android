<?php

//Importing database
require_once('config.php');

//Creating sql query
$sql_modules = "SELECT * FROM modules";
$sql_sub_modules = "SELECT * FROM sub_modules";
$sql_devices = "SELECT * FROM devices";
$sql_devices_type = "SELECT * FROM devices_type";
$sql_access_levels = "SELECT * FROM access_levels";

//getting result
$r_modules = mysqli_query($con, $sql_modules);
$r_sub_modules = mysqli_query($con, $sql_devices);
$r_devices = mysqli_query($con, $sql_devices);
$r_devices_type = mysqli_query($con, $sql_devices_type);
$r_access_levels = mysqli_query($con, $sql_access_levels);

//creating a blank array
$result_modules = array();
$result_sub_modules = array();
$result_devices = array();
$result_devices_type = array();
$result_access_levels = array();
$result = array();

//looping through all the records fetched
while ($row = mysqli_fetch_array($r_modules)) {
    //Pushing name and id in the blank array created
    array_push($result_modules, array(
        "id_module" => $row['id_module'],
        "name" => $row['name'],
        "description" => $row['description'],
        "status" => $row['status'],
        "id_sub_module" => $row['id_sub_module']
    ));
}

while ($row = mysqli_fetch_array($r_sub_modules)) {
    //Pushing name and id in the blank array created
    array_push($result_sub_modules, array(
        "id_sub_module" => $row['id_sub_module'],
        "name" => $row['name'],
        "description" => $row['description']
    ));
}

while ($row = mysqli_fetch_array($r_devices)) {
    //Pushing name and id in the blank array created
    array_push($result_devices, array(
        "id_device" => $row['id_device'],
        "name" => $row['name'],
        "description" => $row['description'],
        "status" => $row['status'],
        "pin" => $row['pin'],
        "value" => $row['value'],
        "id_device_type" => $row['id_device_type'],
        "id_module" => $row['id_module']
    ));
}

while ($row = mysqli_fetch_array($r_devices_type)) {
    //Pushing name and id in the blank array created
    array_push($result_devices_type, array(
        "id_device_type" => $row['id_device_type'],
        "name" => $row['name'],
        "description" => $row['description']
    ));
}

while ($row = mysqli_fetch_array($r_access_levels)) {
    //Pushing name and id in the blank array created
    array_push($result_access_levels, array(
        "id_access_level" => $row['id_access_level'],
        "name" => $row['name'],
        "description" => $row['description']
    ));
}

$result['result_modules'] = $result_modules;
$result['result_sub_modules'] = $result_sub_modules;
$result['result_devices'] = $result_devices;
$result['result_devices_type'] = $result_devices_type;
$result['result_access_levels'] = $result_access_levels;

//Displaying the array in json format
echo json_encode($result);

mysqli_close($con);
?>