<?php

//Getting the requested id
$email = $_GET['email'];

//Importing database
require_once('config.php');
//Creating sql query with where clause to get an specific employee
$sql = "SELECT * FROM users WHERE email='$email'";

//getting result
$r = mysqli_query($con, $sql);

//pushing result to an array
$result = array();
$row = mysqli_fetch_array($r);
array_push($result, array(
    "id_user" => $row['id_user'],
    "email" => $row['email'],
    "password" => $row['password'],
    "surname" => $row['surname'],
    "name" => $row['name'],
    "patronymic" => $row['patronymic'],
    "birthday" => $row['birthday'],
    "gender" => $row['gender'],
    "phone" => $row['phone'],
    "id_access_level" => $row['id_access_level']
));

//displaying in json format
echo json_encode(array('result' => $result));
mysqli_close($con);
?>