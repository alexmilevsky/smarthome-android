<?php

//Importing database
require_once('config.php');

if (isset($_GET['id_module'])) {
    //Creating sql query
    $sql = "SELECT * FROM devices WHERE id_module={$_GET['id_module']}";
} else {
    //Creating sql query
    $sql = "SELECT * FROM devices";
}

//getting result
$r = mysqli_query($con, $sql);

//creating a blank array
$result = array();

//looping through all the records fetched
while ($row = mysqli_fetch_array($r)) {
    //Pushing name and id in the blank array created
    array_push($result, array(
        "id_device" => $row['id_device'],
        "name" => $row['name'],
        "description" => $row['description'],
        "status" => $row['status'],
        "pin" => $row['pin'],
        "value" => $row['value'],
        "id_device_type" => $row['id_device_type'],
        "id_module" => $row['id_module']
    ));
}

//Displaying the array in json format
echo json_encode(array('result_devices' => $result));

mysqli_close($con);
?>