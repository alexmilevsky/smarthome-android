//zoomkat 5-24-10
// http://www.arduino.cc/en/Tutorial/TextString for WString.h
#include <WString.h>
#include <SPI.h>
#include <Ethernet.h>

byte mac[] = {
  0xDE, 0xAD, 0xBE, 0xEF, 0xFE, 0xED
};
IPAddress ip(192, 168, 1, 177);
EthernetServer server(80);  // create a server at port 80

String readString = String(100); //string for fetching data from address

int ind1 = 0;
int ind2 = 0;
//////////////////////

void setup(){
  //start Ethernet
  Ethernet.begin(mac, ip);  // initialize Ethernet device
  server.begin();
  
  pinMode(2, OUTPUT);
  pinMode(7, OUTPUT);
  pinMode(8, OUTPUT);
  
  //enable serial data print 
  Serial.begin(9600); 
}

void loop(){
  // Create a client connection
  EthernetClient client = server.available();
  if (client) {
  while (client.connected()) {
    if (client.available()) {
      char c = client.read();  
      
      //read char by char HTTP request
      if (readString.length() < 100) {  
        //store characters to string 
        readString.concat(c); 
      } 
        
      //if HTTP request has ended
      if (c == '\n') {  
        
         //GET /?PORT=2&VALUE=1 HTTP/1.1
         if(readString.indexOf("PORT") >= 0) {
          // устанавливает значение устройству по запросу PORT=NUMBER&VALUE=NUMBER
          setDevice(client);
         }
         else {
          // генерируем шаблон страницы
          generatePage(client);          
         }
         
     
         // генерируем шаблон страницы
         //generatePage(client);
         // делаем задержку клиенту
         delay(1);
         //stopping client
         client.stop();
      
         /////////////////////
         //clearing string for next read
         readString=""; 
      }
    } // if (client.available()) {
  }  // while
  } // if
} 

// устанавливает значение устройству по запросу PORT=NUMBER&VALUE=NUMBER&STATUS=STATUS
void setDevice(EthernetClient client)
{
  ind1 = readString.indexOf("PORT");
  ind2 = readString.indexOf('&');
  // значение порта после слова PORT= и перед &
  uint8_t PORT  = atoi (readString.substring(ind1+5, ind2).c_str ());   
  // удаляем данные с портом
  readString = readString.substring(ind2+1);
  
  ind1 = readString.indexOf("VALUE");
  ind2 = readString.indexOf('&');
  // значение порта после слова VALUE= и перед &
  String VALUE = readString.substring(ind1+6, ind2);
  // удаляем данные с значением
  readString = readString.substring(ind2+1);

  ind1 = readString.indexOf("STATUS");
  ind2 = readString.indexOf(' ');
  // значение порта после слова STATUS= 
  String STATUS = readString.substring(ind1+7, ind2);

  if (STATUS == "Включен")
  {
    if (VALUE != "0")
    {
      digitalWrite(PORT, HIGH);
    }
    else
    {
      digitalWrite(PORT, LOW);          
    }
  }
  else if (STATUS == "Выключен")
  {
    digitalWrite(PORT, LOW);
  }

  Serial.println("Статус: " + STATUS + " . Значение: " + VALUE);

  client.println("HTTP/1.1 200 OK");
  client.println("Content-Type: text/html");
  client.println("Connection: close");
  client.println();
  client.println("Параметры устройства переданы");;
}

void generatePage(EthernetClient client)
{
  client.println("HTTP/1.1 200 OK");
  client.println("Content-Type: text/html");
  client.println();
  client.println("<head> ");
  client.println("<meta http-equiv='Content-Type' content='text/html; charset=utf-8' /> ");
  client.println("<title>SmartHome</title>");
  client.println("</head> ");
  client.println("<body>");
  client.println("<h3>Введите порт и значение устройства</h3>");
  client.println("<form method='get' id='device-form' action='' method='get'><input type='text' name='PORT' value='' placeholder='Порт устройства'> <input type='text' name='VALUE' value='' placeholder='Значение устройства'> <input type='submit' value='Применить'></form>");
  client.println("<script src='https://code.jquery.com/jquery-3.2.0.min.js' integrity='sha256-JAW99MJVpJBGcbzEuXk4Az05s/XyDdBomFqNlM3ic+I=' crossorigin='anonymous'></script>");
  client.println("<script type='text/javascript'>");
  client.println("$(document).ready(function() {");
  client.println("var form = $('#device-form'); form.submit(function(ev) { $.ajax({type: form.attr('method'), url: form.attr('action'), data: form.serialize(), success: function(data) { } }); ev.preventDefault(); }); ");
  client.println("});");
  client.println("</script>");
  client.println("</body></html>");
}

