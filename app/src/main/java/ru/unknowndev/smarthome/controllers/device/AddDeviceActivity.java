package ru.unknowndev.smarthome.controllers.device;

import android.content.Intent;
import android.os.Bundle;
import android.support.v7.app.AppCompatActivity;
import android.support.v7.widget.Toolbar;
import android.view.Menu;
import android.view.MenuItem;
import android.widget.ArrayAdapter;
import android.widget.EditText;
import android.widget.Spinner;
import android.widget.Toast;

import ru.unknowndev.smarthome.R;
import ru.unknowndev.smarthome.controllers.MainActivity;
import ru.unknowndev.smarthome.models.Device;
import ru.unknowndev.smarthome.models.DevicesType;
import ru.unknowndev.smarthome.models.Operation;
import ru.unknowndev.smarthome.services.DevicesManager;
import ru.unknowndev.smarthome.services.OperationsManager;

public class AddDeviceActivity extends AppCompatActivity {

    // бар меню сверху
    protected Toolbar mActionBarToolbar;
    protected EditText device_name_edit,
            device_description_edit,
            device_pin_edit,
            device_value_edit;

    protected Device device;
    protected int id_module;
    protected Spinner type_device_Spinner;
    protected Spinner statusSpinner;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_add_device);

        // объявляем бар
        mActionBarToolbar = (Toolbar) findViewById(R.id.toolbar_actionbar);
        mActionBarToolbar.setTitle("");
        // задаем настроенный экшн бар
        setSupportActionBar(mActionBarToolbar);
        // добавляем к бару кнопку назад
        if (getSupportActionBar() != null){
            getSupportActionBar().setDisplayHomeAsUpEnabled(true);
            getSupportActionBar().setDisplayShowHomeEnabled(true);
        }

        Intent intent = getIntent();
        id_module = intent.getIntExtra("id_module", -1);

        device_name_edit = (EditText)findViewById(R.id.device_name_edit);
        device_description_edit = (EditText)findViewById(R.id.device_description_edit);
        device_pin_edit = (EditText)findViewById(R.id.device_pin_edit);
        device_value_edit = (EditText)findViewById(R.id.device_value_edit);

        initTypeDeviceSpinner();
        initStatusSpinner();
    }

    /**
     * Устанавливает значения переключателю
     */
    private void initStatusSpinner() {
        statusSpinner = (Spinner) findViewById(R.id.device_status_spinner);
        // Создаем адаптер ArrayAdapter с помощью массива строк и стандартной разметки элемета spinner
        ArrayAdapter<CharSequence> adapter = ArrayAdapter.createFromResource(this,
                R.array.device_status_enum, android.R.layout.simple_spinner_item);
        // Определяем разметку для использования при выборе элемента
        adapter.setDropDownViewResource(android.R.layout.simple_spinner_dropdown_item);
        // Применяем адаптер к элементу spinner
        statusSpinner.setAdapter(adapter);
    }

    /**
     * Устанавливает значения переключателю
     */
    private void initTypeDeviceSpinner() {
        type_device_Spinner = (Spinner) findViewById(R.id.device_type_spinner);
        // Создаем адаптер ArrayAdapter с помощью массива строк и стандартной разметки элемета spinner
        String[] names = new String[MainActivity.devicesTypes.size()];
        // перебираем из массива типа и генерируем имена
        for (int i = 0; i < names.length; i++)
        {
            DevicesType devicesType = MainActivity.devicesTypes.get(i);
            names[i] = devicesType.getName();
        }
        ArrayAdapter<String> adapter = new ArrayAdapter<String>(this, android.R.layout.simple_list_item_1, names);
        // Определяем разметку для использования при выборе элемента
        adapter.setDropDownViewResource(android.R.layout.simple_spinner_dropdown_item);
        // Применяем адаптер к элементу spinner
        type_device_Spinner.setAdapter(adapter);
    }

    @Override
    public boolean onCreateOptionsMenu(Menu menu) {
        getMenuInflater().inflate(R.menu.device_control_menu, menu);
        return true;
    }

    @Override
    public boolean onOptionsItemSelected(MenuItem item) {

        int itemId = item.getItemId();

        if (itemId == R.id.action_delete) {
            deleteClick();
        }
        else if (itemId == R.id.action_save) {
            saveClick();
        } else if (item.getItemId() == android.R.id.home) {
            finish();
        }
        return super.onOptionsItemSelected(item);
    }

    private void deleteClick()
    {
        // очищать поля
        device_name_edit.setText("");
        device_description_edit.setText("");
        device_pin_edit.setText("");
        device_value_edit.setText("");
    }

    private void saveClick()
    {
        String name = device_name_edit.getText().toString();
        String description = device_description_edit.getText().toString();
        String status = statusSpinner.getSelectedItem().toString();
        int pin = Integer.parseInt(device_pin_edit.getText().toString());
        String value = device_value_edit.getText().toString();

        String name_type = type_device_Spinner.getSelectedItem().toString();
        int id_device_type = -1;

        // ищем id в списке
        for(DevicesType devicesType : MainActivity.devicesTypes)
        {
            if (devicesType.getName().equals(name_type))
            {
                id_device_type = devicesType.getId_device_type();
                break;
            }
        }

        device = new Device(name, description, status, pin, value, id_device_type, id_module);
        DevicesManager.add(device, this);
    }

    public void ReturnThreadResult(String s) {

        if (Integer.parseInt(s) != -1)
        {
            device.setId_device(Integer.parseInt(s));
            MainActivity.devices.add(device);

            // регистрируем операцию о добавлении устройства
            Operation operationAdd = new Operation("Устройство " + device.getName() + " было добавлено",
                    "",
                    id_module,
                    device.getId_device(),
                    MainActivity.user.getId_user(),
                    -1);
            OperationsManager.add(operationAdd);

            Toast toast = Toast.makeText(getApplicationContext(), "Устройство добавлено", Toast.LENGTH_SHORT);
            toast.show();

            Intent intent = new Intent(getApplicationContext(), MainActivity.class);
            startActivity(intent);
            //this.finish();
        }
        else {
            Toast toast = Toast.makeText(getApplicationContext(), "Произошла ошибка", Toast.LENGTH_SHORT);
            toast.show();
        }
    }
}
