package ru.unknowndev.smarthome.controllers.device;

import android.content.Intent;
import android.os.Bundle;
import android.support.v7.app.AppCompatActivity;
import android.support.v7.widget.Toolbar;
import android.view.Menu;
import android.view.MenuItem;
import android.view.View;
import android.widget.AdapterView;
import android.widget.AdapterView.OnItemClickListener;
import android.widget.ArrayAdapter;
import android.widget.CompoundButton;
import android.widget.ListView;
import android.widget.Switch;
import android.widget.TextView;
import android.widget.Toast;

import ru.unknowndev.smarthome.R;
import ru.unknowndev.smarthome.controllers.MainActivity;
import ru.unknowndev.smarthome.models.Device;
import ru.unknowndev.smarthome.models.Module;
import ru.unknowndev.smarthome.models.Operation;
import ru.unknowndev.smarthome.services.DevicesManager;
import ru.unknowndev.smarthome.services.ModulesManager;
import ru.unknowndev.smarthome.services.OperationsManager;

public abstract class DevicesActivity extends AppCompatActivity {

    // бар меню сверху
    protected Toolbar mActionBarToolbar;
    protected TextView textSelect;
    protected ListView listview;
    protected Module module;
    protected Switch status_switch;
    private DevicesActivity context;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_devices);

        Intent intent = getIntent();
        module = ModulesManager.getModuleById(intent.getIntExtra("id_module", -1));

        initSwitch();

        // объявляем бар
        mActionBarToolbar = (Toolbar) findViewById(R.id.toolbar_actionbar);
        mActionBarToolbar.setTitle("");
        // задаем настроенный экшн бар
        setSupportActionBar(mActionBarToolbar);
        // добавляем к бару кнопку назад
        if (getSupportActionBar() != null){
            getSupportActionBar().setDisplayHomeAsUpEnabled(true);
            getSupportActionBar().setDisplayShowHomeEnabled(true);
        }

        textSelect = (TextView)findViewById(R.id.device_edit_text);

        initListView();

        if (module.getStatus().equals("Выключен")) {
            textSelect.setEnabled(false);
            listview.setClickable(false);
            listview.setAlpha(0.3f);
        }

        context = this;
    }

    @Override
    public boolean onCreateOptionsMenu(Menu menu) {
        getMenuInflater().inflate(R.menu.device_menu, menu);
        return true;
    }

    @Override
    public boolean onOptionsItemSelected(MenuItem item) {

        int itemId = item.getItemId();

        if (itemId == R.id.action_add) {
            addClick();
        }
        else if (itemId == R.id.action_refresh) {
            refreshClick();
        }
        else if (item.getItemId() == android.R.id.home) {
            finish();
        }
        return super.onOptionsItemSelected(item);
    }

    private void refreshClick()
    {
        DevicesManager.getDevices(this);
    }

    private void addClick()
    {
        Intent intent = new Intent(getApplicationContext(), AddDeviceActivity.class);
        intent.putExtra("id_module", module.getId_module());
        startActivity(intent);
    }

    private void initSwitch()
    {
        status_switch = (Switch) findViewById(R.id.activity_switch);
        status_switch.setOnCheckedChangeListener (null);
        status_switch.setChecked(module.getStatus().equals("Включен"));
        status_switch.setOnCheckedChangeListener(new CompoundButton.OnCheckedChangeListener() {
            public void onCheckedChanged(CompoundButton buttonView, boolean isChecked) {
                if (status_switch.isChecked())
                {
                    module.setStatus("Включен");
                    textSelect.setEnabled(true);
                    listview.setAlpha(1f);
                    ModulesManager.update(module, context);
                }
                else
                {
                    module.setStatus("Выключен");
                    textSelect.setEnabled(false);
                    listview.setAlpha(0.3f);
                    ModulesManager.update(module, context);
                }
            }
        });
    }

    private void initListView() {
        // находим список
        listview = (ListView) findViewById(R.id.devices_list_view);
        listview.setOnItemClickListener(listViewListener);

        if (MainActivity.devices != null)
        {
            String[] names = DevicesManager.generateNamesForDevices(DevicesManager.countDevicesInModule(module.getId_module()),
                                                   module.getId_module()
            );
            ArrayAdapter<String> adapter = new ArrayAdapter<String>(this, android.R.layout.simple_list_item_1, names);
            listview.setAdapter(adapter);

            if (names.length < 1)
            {
                // тест, что нужно добавить устройства
                textSelect.setText(this.getString(R.string.device_add_text));
            }
        }
        else {
            // тест, что нужно добавить устройства
            textSelect.setText(this.getString(R.string.device_add_text));
        }
    }

    /**
     * Слушатель для списка
     */
    public AdapterView.OnItemClickListener listViewListener = new OnItemClickListener() {
        public void onItemClick(AdapterView<?> parent, View view, int position, long id) {
            if (module.getStatus().equals("Включен"))
            {
                String selectedFromList =(String) (listview.getItemAtPosition(position));

                for(Device device : MainActivity.devices)
                {
                    if (device.toString().equals(selectedFromList.toString())) {
                        Intent intent = new Intent(getApplicationContext(), ControlDeviceActivity.class);
                        intent.putExtra("id_device", device.getId_device());
                        startActivity(intent);
                        break;
                    }
                }
            }
        }
    };

    public void ReturnThreadResult(String s) {

        if (s.contains("result_devices"))
        {
            MainActivity.devices = DevicesManager.extractDevicesFromJSON(s);
            int countDeviceOfModule = DevicesManager.countDevicesInModule(module.getId_module());
            // задаем массив слов из названий устройств
            String[] names = DevicesManager.generateNamesForDevices(countDeviceOfModule, module.getId_module());
            // создаем адаптер
            ArrayAdapter<String> adapter = new ArrayAdapter<String>(this,
                    android.R.layout.simple_list_item_1, names);
            // присваиваем адаптер списку
            listview.setAdapter(adapter);

            Toast toast = Toast.makeText(getApplicationContext(),
                    "Данные обновлены", Toast.LENGTH_SHORT);
            toast.show();
        }

        if (s.contains("Модуль обновлен"))
        {
            int indexInList = module.getIndexInList();
            MainActivity.modules.remove(indexInList);
            MainActivity.modules.add(module);

            // регистрируем операцию о обновлении модуля
            Operation operationUpdate = new Operation("Модуль " + module.getName() + ": " + module.getStatus(),
                                                "",
                                                module.getId_module(),
                                                -1,
                                                MainActivity.user.getId_user(),
                                                -1);
            OperationsManager.add(operationUpdate);

            Toast toast = Toast.makeText(getApplicationContext(),
                    "Модуль обновлен", Toast.LENGTH_SHORT);
            toast.show();
        }

    }
}
