package ru.unknowndev.smarthome.controllers.user;

import android.content.Intent;
import android.os.Bundle;
import android.support.v7.app.AppCompatActivity;
import android.support.v7.widget.Toolbar;
import android.view.Menu;
import android.view.MenuItem;
import android.view.View;
import android.widget.Button;
import android.widget.EditText;
import android.widget.RadioGroup;
import android.widget.Toast;

import ru.unknowndev.smarthome.R;
import ru.unknowndev.smarthome.models.User;
import ru.unknowndev.smarthome.services.UsersManager;

public class RegistrationActivity extends AppCompatActivity {

    private EditText editEmail, editPassword, editSurname, editName, editPatronymic, editBirthday, editPhone;
    protected Toolbar mActionBarToolbar;
    private RadioGroup genderGroup;
    private Button buttonRegister;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_registration);

        // получаем поля
        editEmail = (EditText)findViewById(R.id.email_text);
        editPassword = (EditText)findViewById(R.id.password_text);
        editSurname = (EditText)findViewById(R.id.surname_text);
        editName = (EditText)findViewById(R.id.name_text);
        editPatronymic = (EditText)findViewById(R.id.patronymic_text);
        editBirthday = (EditText)findViewById(R.id.birthday_text);
        editPhone = (EditText)findViewById(R.id.phone_text);
        genderGroup = (RadioGroup)findViewById(R.id.gender_radiogroup);
        // получаем поля
        buttonRegister = (Button)findViewById(R.id.sign_in_button);
        buttonRegister.setOnClickListener(listenerRegister);

        // объявляем бар
        mActionBarToolbar = (Toolbar) findViewById(R.id.toolbar_actionbar);
        mActionBarToolbar.setTitle("");
        // задаем настроенный экшн бар
        setSupportActionBar(mActionBarToolbar);
        // добавляем к бару кнопку назад
        if (getSupportActionBar() != null){
            getSupportActionBar().setDisplayHomeAsUpEnabled(true);
            getSupportActionBar().setDisplayShowHomeEnabled(true);
        }
    }

    @Override
    public boolean onCreateOptionsMenu(Menu menu) {
        getMenuInflater().inflate(R.menu.back_menu, menu);
        return true;
    }

    @Override
    public boolean onOptionsItemSelected(MenuItem item) {
        if (item.getItemId() == android.R.id.home) {
            finish();
        }
        return super.onOptionsItemSelected(item);
    }

    // создаем обработчик нажатия
    View.OnClickListener listenerRegister = new View.OnClickListener() {
        @Override
        public void onClick(View v) {
            addUser();
        }
    };

    /**
     * Registration click
     *
     * @param view
     */
    public void registerClick(View view)
    {
        addUser();
    }


    private void addUser(){

        final String email = editEmail.getText().toString().trim();
        final String password = editPassword.getText().toString().trim();
        final String surname = editSurname.getText().toString().trim();
        final String name = editName.getText().toString().trim();
        final String patronymic = editPatronymic.getText().toString().trim();
        final String birthday = editBirthday.getText().toString().trim();
        final String gender = genderGroup.getCheckedRadioButtonId() == R.id.gender_female_radiobutton ? "Женский" : "Мужской";
        final String phone = editPhone.getText().toString().trim();

        User user = new User(email, password, surname, name, patronymic, birthday, gender, phone, 2);
        UsersManager.add(user, this);
    }

    /**
     * Result from async
     *
     * @param result
     */
    public void ReturnThreadResult(String result)
    {
        if (result.equals("Пользователь зарегистрирован"))
        {
            Toast.makeText(RegistrationActivity.this,result,Toast.LENGTH_LONG).show();
            Intent intent = new Intent(RegistrationActivity.this, LoginActivity.class);
            startActivity(intent);
        }

        if (result.equals("Пользователь уже зарегистрирован"))
        {
            Toast.makeText(RegistrationActivity.this,result,Toast.LENGTH_LONG).show();
        }

        if (result.equals("Ошибка"))
        {
            Toast.makeText(RegistrationActivity.this,result,Toast.LENGTH_LONG).show();
        }
    }
}
