package ru.unknowndev.smarthome.controllers;

import android.content.Intent;
import android.content.SharedPreferences;
import android.os.Bundle;
import android.support.v7.app.AppCompatActivity;
import android.support.v7.widget.Toolbar;
import android.view.Menu;
import android.view.MenuItem;
import android.view.View;
import android.widget.ImageButton;
import android.widget.Toast;

import java.util.ArrayList;
import java.util.List;

import ru.unknowndev.smarthome.R;
import ru.unknowndev.smarthome.controllers.main.ClimateActivity;
import ru.unknowndev.smarthome.controllers.main.EnergyActivity;
import ru.unknowndev.smarthome.controllers.main.FunActivity;
import ru.unknowndev.smarthome.controllers.main.LightActivity;
import ru.unknowndev.smarthome.controllers.main.ReportsActivity;
import ru.unknowndev.smarthome.controllers.main.SecurityActivity;
import ru.unknowndev.smarthome.controllers.main.SettingsActivity;
import ru.unknowndev.smarthome.controllers.user.LoginActivity;
import ru.unknowndev.smarthome.controllers.user.ProfileActivity;
import ru.unknowndev.smarthome.models.AccessLevels;
import ru.unknowndev.smarthome.models.Device;
import ru.unknowndev.smarthome.models.DevicesType;
import ru.unknowndev.smarthome.models.Module;
import ru.unknowndev.smarthome.models.SubModule;
import ru.unknowndev.smarthome.models.User;
import ru.unknowndev.smarthome.services.DataManager;
import ru.unknowndev.smarthome.services.DevicesManager;
import ru.unknowndev.smarthome.services.ModulesManager;
import ru.unknowndev.smarthome.services.Settings;
import ru.unknowndev.smarthome.services.UsersManager;

public class MainActivity extends AppCompatActivity {

    // кнопки на главной странице
    private ImageButton light, climate, energy, security, fun, report;
    // бар меню сверху
    Toolbar mActionBarToolbar;
    // настройки приложения
    public static SharedPreferences settings_app;
    // объекты устройств
    public static List<Device> devices;
    // объекты типа устройств
    public static List<DevicesType> devicesTypes;
    // объекты модулей
    public static List<Module> modules;
    // объекты модулей
    public static List<SubModule> subModules;
    // объекты прав доступа
    public static List<AccessLevels> accessLevels;
    // текущий пользователь
    public static User user;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_main);

        if (devices == null) devices = new ArrayList<Device>();
        if (devicesTypes == null) devicesTypes = new ArrayList<DevicesType>();
        if (modules == null) modules = new ArrayList<Module>();
        if (subModules == null) subModules = new ArrayList<SubModule>();
        if (accessLevels == null) accessLevels = new ArrayList<AccessLevels>();


        mActionBarToolbar = (Toolbar) findViewById(R.id.toolbar_actionbar);
        // получаем объект настроек
        settings_app = Settings.getSharedPreferences(this.getApplicationContext());
        // получение кнопок
        getButtons();

        // если пользователь не авторизован, то отключаем взаимодействие с модулями и делаем им прозрачность
        if (user == null)
        {
            light.setEnabled(false);
            climate.setEnabled(false);
            energy.setEnabled(false);
            security.setEnabled(false);
            fun.setEnabled(false);
            report.setEnabled(false);

            light.setAlpha(0.3f);
            climate.setAlpha(0.3f);
            energy.setAlpha(0.3f);
            security.setAlpha(0.3f);
            fun.setAlpha(0.3f);
            report.setAlpha(0.3f);

            // если пользователь не задан, выводим сообщение о необходимости авторизации
            Toast toast = Toast.makeText(getApplicationContext(), "Авторизуйтесь для доступа к модулям", Toast.LENGTH_SHORT);
            toast.show();
        }
        else
        {
            mActionBarToolbar.setTitle(user.getName());
        }

        // если настроек меньше 1, то выводим сообщение о настройках
        if (settings_app.getAll().size() < 1)
        {
            Toast toast = Toast.makeText(getApplicationContext(), "Необходимо настроить приложение в модуле настроек", Toast.LENGTH_SHORT);
            toast.show();
        }

        // задаем настроенный экшн бар
        setSupportActionBar(mActionBarToolbar);

        // если пользователь получен и количество прав доступа меньше 1, тогда получить все данные
        if (user != null && (accessLevels.size() < 1)) DataManager.getAll(this);
    }

    @Override
    public boolean onCreateOptionsMenu(Menu menu) {
        getMenuInflater().inflate(R.menu.main_menu, menu);

        // если пользователь не авторизован
        if (user == null) menu.findItem(R.id.action_refresh).setVisible(false);

        return true;
    }

    @Override
    public boolean onOptionsItemSelected(MenuItem item) {

        int itemId = item.getItemId();

        if (itemId == R.id.action_refresh) {
            DataManager.getAll(this);
        }
        else if (itemId == R.id.action_settings) {
            Intent intent = new Intent(MainActivity.this, SettingsActivity.class);
            startActivity(intent);
        }
        return super.onOptionsItemSelected(item);
    }

    public void accountButton(View view)
    {
        Intent intent;
        // проверять если не авторизован, запускать окно логина, иначе выводить профиль
        if (user == null)
        {
            intent = new Intent(MainActivity.this, LoginActivity.class);
            startActivity(intent);
        }
        else
        {
            intent = new Intent(MainActivity.this, ProfileActivity.class);
            startActivity(intent);
        }
    }

    public void lightButton(View view)
    {
        Module module = ModulesManager.getModuleByName("Управление светом");

        if (module != null)
        {
            int id = module.getId_module();

            Intent intent = new Intent(MainActivity.this, LightActivity.class);
            intent.putExtra("id_module", id);
            startActivity(intent);
        }
        else
        {
            Toast toast = Toast.makeText(getApplicationContext(),
                    "Обновите данные", Toast.LENGTH_SHORT);
            toast.show();
        }
    }

    public void climateButton(View view)
    {
        Module module = ModulesManager.getModuleByName("Управление климатом");

        if (module != null)
        {
            int id = module.getId_module();

            Intent intent = new Intent(MainActivity.this, ClimateActivity.class);
            intent.putExtra("id_module", id);
            startActivity(intent);
        }
        else
        {
            Toast toast = Toast.makeText(getApplicationContext(),
                    "Обновите данные", Toast.LENGTH_SHORT);
            toast.show();
        }
    }

    public void energyButton(View view)
    {
        Module module = ModulesManager.getModuleByName("Управление электроэнергией");

        if (module != null)
        {
            int id = module.getId_module();

            Intent intent = new Intent(MainActivity.this, EnergyActivity.class);
            intent.putExtra("id_module", id);
            startActivity(intent);
        }
        else
        {
            Toast toast = Toast.makeText(getApplicationContext(),
                    "Обновите данные", Toast.LENGTH_SHORT);
            toast.show();
        }
    }

    public void securityButton(View view)
    {
        Module module = ModulesManager.getModuleByName("Управление безопасностью");

        if (module != null)
        {
            int id = module.getId_module();

            Intent intent = new Intent(MainActivity.this, SecurityActivity.class);
            intent.putExtra("id_module", id);
            startActivity(intent);
        }
        else
        {
            Toast toast = Toast.makeText(getApplicationContext(),
                    "Обновите данные", Toast.LENGTH_SHORT);
            toast.show();
        }
    }

    public void funButton(View view)
    {
        Module module = ModulesManager.getModuleByName("Управление развлечениями");

        if (module != null)
        {
            int id = module.getId_module();

            Intent intent = new Intent(MainActivity.this, FunActivity.class);
            intent.putExtra("id_module", id);
            startActivity(intent);
        }
        else
        {
            Toast toast = Toast.makeText(getApplicationContext(),
                    "Обновите данные", Toast.LENGTH_SHORT);
            toast.show();
        }
    }

    public void reportButton(View view)
    {
        Intent intent = new Intent(MainActivity.this, ReportsActivity.class);
        startActivity(intent);
    }

    private void getButtons()
    {
        // кнопка света
        light = (ImageButton)findViewById(R.id.lightButton);
        // кнопка климат
        climate = (ImageButton)findViewById(R.id.climateButton);
        // кнопка энергии
        energy = (ImageButton)findViewById(R.id.energyButton);
        // кнопка безопасности
        security = (ImageButton)findViewById(R.id.securityButton);
        // кнопка развлечений
        fun = (ImageButton)findViewById(R.id.funButton);
        // кнопка отчетов
        report = (ImageButton)findViewById(R.id.reportButton);
    }

    public void ReturnThreadResult(String s) {
        if (s.contains("result_modules"))
        {
            modules = ModulesManager.extractModulesFromJSON(s);

            Toast toast = Toast.makeText(getApplicationContext(),
                    "Данные модулей обновлены", Toast.LENGTH_SHORT);
            toast.show();
        }

        if (s.contains("result_sub_modules"))
        {
            subModules = ModulesManager.extractModulesTypesFromJSON(s);
        }

        if (s.contains("result_devices"))
        {
            devices = DevicesManager.extractDevicesFromJSON(s);

            Toast toast = Toast.makeText(getApplicationContext(),
                    "Данные устройств обновлены", Toast.LENGTH_SHORT);
            toast.show();
        }

        if (s.contains("result_devices_type"))
        {
            devicesTypes = DevicesManager.extractDevicesTypeFromJSON(s);
        }

        if (s.contains("result_access_levels"))
        {
            accessLevels = UsersManager.extractAccessLevelsFromJSON(s);
        }

    }
}
