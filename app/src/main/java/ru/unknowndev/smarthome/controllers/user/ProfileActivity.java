package ru.unknowndev.smarthome.controllers.user;

import android.content.Intent;
import android.os.Bundle;
import android.support.v7.app.AppCompatActivity;
import android.support.v7.widget.Toolbar;
import android.view.Menu;
import android.view.MenuItem;
import android.view.View;
import android.widget.EditText;
import android.widget.RadioGroup;
import android.widget.TextView;
import android.widget.Toast;

import ru.unknowndev.smarthome.R;
import ru.unknowndev.smarthome.controllers.MainActivity;
import ru.unknowndev.smarthome.models.User;
import ru.unknowndev.smarthome.services.UsersManager;

public class ProfileActivity extends AppCompatActivity {

    private EditText editPassword, editSurname, editName, editPatronymic, editBirthday, editPhone;
    protected Toolbar mActionBarToolbar;
    private TextView textEmail;
    private RadioGroup genderGroup;

    private User user;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_profile);

        // получаем поля
        textEmail = (TextView)findViewById(R.id.email_text);
        editPassword = (EditText)findViewById(R.id.password_text);
        editSurname = (EditText)findViewById(R.id.surname_text);
        editName = (EditText)findViewById(R.id.name_text);
        editPatronymic = (EditText)findViewById(R.id.patronymic_text);
        editBirthday = (EditText)findViewById(R.id.birthday_text);
        editPhone = (EditText)findViewById(R.id.phone_text);
        genderGroup = (RadioGroup)findViewById(R.id.gender_radiogroup);

        textEmail.setText(MainActivity.user.getEmail());
        editPassword.setText(MainActivity.user.getPassword());
        editSurname.setText(MainActivity.user.getSurname());
        editName.setText(MainActivity.user.getName());
        editPatronymic.setText(MainActivity.user.getPatronymic());
        editBirthday.setText(MainActivity.user.getBirthday());
        editPhone.setText(MainActivity.user.getPhone());
        genderGroup.check(MainActivity.user.getGender().equals("Мужской") ? R.id.gender_male_radiobutton : R.id.gender_female_radiobutton);
        // получаем поля

        // объявляем бар
        mActionBarToolbar = (Toolbar) findViewById(R.id.toolbar_actionbar);
        mActionBarToolbar.setTitle("");
        // задаем настроенный экшн бар
        setSupportActionBar(mActionBarToolbar);
        // добавляем к бару кнопку назад
        if (getSupportActionBar() != null){
            getSupportActionBar().setDisplayHomeAsUpEnabled(true);
            getSupportActionBar().setDisplayShowHomeEnabled(true);
        }
    }

    @Override
    public boolean onCreateOptionsMenu(Menu menu) {
        getMenuInflater().inflate(R.menu.back_menu, menu);
        return true;
    }

    @Override
    public boolean onOptionsItemSelected(MenuItem item) {
        if (item.getItemId() == android.R.id.home) {
            finish();
        }
        return super.onOptionsItemSelected(item);
    }

    /**
     * Обработка клика сохранить
     *
     * @param view
     */
    public void saveClick(View view)
    {
        updateUser();
    }

    /**
     * Обработка клика выйти
     *
     * @param view
     */
    public void signOutClick(View view)
    {
        // обнуляем текущего пользователя
        MainActivity.user = null;

        Toast.makeText(ProfileActivity.this,"Сеанс завершен",Toast.LENGTH_LONG).show();
        Intent intent = new Intent(ProfileActivity.this, MainActivity.class);
        startActivity(intent);
    }

    /**
     * Стартовый процесс обновления данных пользователя
     *
     */
    private void updateUser(){

        final String password = editPassword.getText().toString().trim();
        final String surname = editSurname.getText().toString().trim();
        final String name = editName.getText().toString().trim();
        final String patronymic = editPatronymic.getText().toString().trim();
        final String birthday = editBirthday.getText().toString().trim();
        final String gender = genderGroup.getCheckedRadioButtonId() == R.id.gender_female_radiobutton ? "Женский" : "Мужской";
        final String phone = editPhone.getText().toString().trim();

        user = new User(MainActivity.user.getId_user(), MainActivity.user.getEmail(), password, surname, name, patronymic, birthday, gender, phone, MainActivity.user.getId_group());

        // если новые данные пользователя не равны текущим, то обновляем
        if (MainActivity.user != user)
        {
            UsersManager.update(user, this);
        }
        else {
            Toast.makeText(ProfileActivity.this,"Вы не изменили данные",Toast.LENGTH_LONG).show();
        }
    }

    /**
     * Result from async
     *
     * @param result
     */
    public void ReturnThreadResult(String result)
    {
        if (result.contains("Данные обновлены"))
        {
            MainActivity.user = user;

            Toast.makeText(ProfileActivity.this,result,Toast.LENGTH_LONG).show();
            Intent intent = new Intent(ProfileActivity.this, MainActivity.class);
            startActivity(intent);
        }

        if (result.equals("Ошибка"))
        {
            Toast.makeText(ProfileActivity.this,result,Toast.LENGTH_LONG).show();
        }
    }
}
