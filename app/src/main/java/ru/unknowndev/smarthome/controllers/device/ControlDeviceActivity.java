package ru.unknowndev.smarthome.controllers.device;

import android.content.Intent;
import android.os.Bundle;
import android.support.v7.app.AppCompatActivity;
import android.support.v7.widget.Toolbar;
import android.view.Menu;
import android.view.MenuItem;
import android.widget.ArrayAdapter;
import android.widget.EditText;
import android.widget.Spinner;
import android.widget.TextView;
import android.widget.Toast;

import ru.unknowndev.smarthome.R;
import ru.unknowndev.smarthome.controllers.MainActivity;
import ru.unknowndev.smarthome.models.Device;
import ru.unknowndev.smarthome.models.Operation;
import ru.unknowndev.smarthome.services.DevicesManager;
import ru.unknowndev.smarthome.services.OperationsManager;
import ru.unknowndev.smarthome.services.Settings;

public class ControlDeviceActivity extends AppCompatActivity {

    // бар меню сверху
    protected Toolbar mActionBarToolbar;
    protected TextView device_info_text;
    protected EditText device_value_edit;
    protected Spinner statusSpinner;

    protected Device device;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_control_device);

        // объявляем бар
        mActionBarToolbar = (Toolbar) findViewById(R.id.toolbar_actionbar);
        mActionBarToolbar.setTitle("");
        // задаем настроенный экшн бар
        setSupportActionBar(mActionBarToolbar);
        // добавляем к бару кнопку назад
        if (getSupportActionBar() != null){
            getSupportActionBar().setDisplayHomeAsUpEnabled(true);
            getSupportActionBar().setDisplayShowHomeEnabled(true);
        }

        device_info_text = (TextView) findViewById(R.id.device_info_text);
        device_value_edit = (EditText) findViewById(R.id.device_value_edit);

        Intent intent = getIntent();
        int id_device = intent.getIntExtra("id_device", -1);

        // получаем устройство по id
        device = DevicesManager.getDeviceById(id_device);

        device_info_text.setText(device.toString());
        device_value_edit.setText(device.getValue());

        initStatusSpinner();
    }

    /**
     * Устанавливает значения переключателю
     */
    private void initStatusSpinner() {
        statusSpinner = (Spinner) findViewById(R.id.device_status_spinner);
        // Создаем адаптер ArrayAdapter с помощью массива строк и стандартной разметки элемета spinner
        ArrayAdapter<CharSequence> adapter = ArrayAdapter.createFromResource(this,
                R.array.device_status_enum, android.R.layout.simple_spinner_item);
        // Определяем разметку для использования при выборе элемента
        adapter.setDropDownViewResource(android.R.layout.simple_spinner_dropdown_item);
        // Применяем адаптер к элементу spinner
        statusSpinner.setAdapter(adapter);

        // установка статуса у девайса
        statusSpinner.setSelection(adapter.getPosition(device.getStatus()));
    }

    @Override
    public boolean onCreateOptionsMenu(Menu menu) {
        getMenuInflater().inflate(R.menu.device_control_menu, menu);
        return true;
    }

    @Override
    public boolean onOptionsItemSelected(MenuItem item) {

        int itemId = item.getItemId();

        if (itemId == R.id.action_delete) {
            deleteClick();
        }
        else if (itemId == R.id.action_save) {
            saveClick();
        } else if (item.getItemId() == android.R.id.home) {
            finish();
        }
        return super.onOptionsItemSelected(item);
    }

    private void deleteClick()
    {
        DevicesManager.remove(device, this);
    }

    private void saveClick()
    {
        // прячем клавиатуру
        Settings.hideKeyboard(this);

        String status = statusSpinner.getSelectedItem().toString();
        String value = device_value_edit.getText().toString();

        // если введенные значения не равны текущим
        if (!value.equals(device.getValue()) || !status.equals(device.getStatus()))
        {
            device.setValue(value);
            device.setStatus(status);
            DevicesManager.setValueOnDevice(device, this);
        }
    }

    public void ReturnThreadResult(String s) {

        if (s.contains("Параметры устройства переданы"))
        {
            int index = device.getIndexInList();
            // удаляем устройство со старыми значениями
            MainActivity.devices.remove(index);
            // добавляем с новыми
            MainActivity.devices.add(device);
            // обновляем данные бд
            DevicesManager.update(device, this);
        }

        if (s.equals("Устройство обновлено"))
        {
            /*
            // регистрируем операцию о удалении устройства
            Operation operationUpdate = new Operation("Устройство '" + device.getName() + "' было обновлено",
                    "",
                    device.getId_module(),
                    device.getId_device(),
                    MainActivity.user.getId_user(),
                    -1);
            OperationsManager.add(operationUpdate);
            */

            // регистрируем операцию о обновлении модуля
            Operation operationUpdate = new Operation("Устройство \"" + device.getName() + "\" было обновлено",
                    "",
                    device.getId_module(),
                    device.getId_device(),
                    MainActivity.user.getId_user(),
                    -1);
            OperationsManager.add(operationUpdate);

            Toast toast = Toast.makeText(getApplicationContext(), "Устройство обновлено", Toast.LENGTH_SHORT);
            toast.show();
        }

        if (s.equals("Устройство удалено"))
        {
            int index = device.getIndexInList();
            MainActivity.devices.remove(index);

            // регистрируем операцию о удалении устройства
            Operation operationRemove = new Operation("Устройство \"" + device.getName() + "\" было удалено",
                    "",
                    device.getId_module(),
                    device.getId_device(),
                    MainActivity.user.getId_user(),
                    -1);
            OperationsManager.add(operationRemove);

            Toast toast = Toast.makeText(getApplicationContext(), "Устройство удалено", Toast.LENGTH_SHORT);
            toast.show();

            Intent intent = new Intent(getApplicationContext(), MainActivity.class);
            startActivity(intent);
        }

        if (s.equals("Ошибка")) {
            Toast toast = Toast.makeText(getApplicationContext(), "Произошла ошибка", Toast.LENGTH_SHORT);
            toast.show();
        }

        if (s.equals("Удаление выполнено"))
        {

        }
    }
}
