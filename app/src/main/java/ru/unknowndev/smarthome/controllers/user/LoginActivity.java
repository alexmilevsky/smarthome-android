package ru.unknowndev.smarthome.controllers.user;

import android.content.Intent;
import android.os.Bundle;
import android.support.v7.app.AppCompatActivity;
import android.support.v7.widget.Toolbar;
import android.view.Menu;
import android.view.MenuItem;
import android.view.View;
import android.widget.EditText;
import android.widget.Toast;

import ru.unknowndev.smarthome.R;
import ru.unknowndev.smarthome.controllers.MainActivity;
import ru.unknowndev.smarthome.models.Operation;
import ru.unknowndev.smarthome.models.User;
import ru.unknowndev.smarthome.services.OperationsManager;
import ru.unknowndev.smarthome.services.UsersManager;

public class LoginActivity extends AppCompatActivity {

    // поля ввода
    private EditText editEmail, editPassword, editKeyAccess;
    protected Toolbar mActionBarToolbar;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_login);

        // получаем поля
        editEmail = (EditText)findViewById(R.id.email_text);
        editPassword = (EditText)findViewById(R.id.password_text);
        editKeyAccess = (EditText)findViewById(R.id.key_access_text);
        // получаем поля

        // объявляем бар
        mActionBarToolbar = (Toolbar) findViewById(R.id.toolbar_actionbar);
        mActionBarToolbar.setTitle("");
        // задаем настроенный экшн бар
        setSupportActionBar(mActionBarToolbar);
        // добавляем к бару кнопку назад
        if (getSupportActionBar() != null){
            getSupportActionBar().setDisplayHomeAsUpEnabled(true);
            getSupportActionBar().setDisplayShowHomeEnabled(true);
        }
    }

    @Override
    public boolean onCreateOptionsMenu(Menu menu) {
        getMenuInflater().inflate(R.menu.back_menu, menu);
        return true;
    }

    @Override
    public boolean onOptionsItemSelected(MenuItem item) {
        if (item.getItemId() == android.R.id.home) {
            finish();
        }
        return super.onOptionsItemSelected(item);
    }

    // click on login
    public void signinClick(View view)
    {
        // если все успешно, то на главное меню и уведомление
        //if (controller.isCanUserLogging("admin@smart.home", "smarthome01")) {
        String userEmail = editEmail.getText().toString();
        String userPassword = editPassword.getText().toString();
        String keyAccess = editKeyAccess.getText().toString();

        // запуск авторизации пользователя
        UsersManager.authorizate(userEmail, userPassword, keyAccess, this);
    }

    // click on registration
    public void signupClick(View view)
    {
        Intent intent = new Intent(LoginActivity.this, RegistrationActivity.class);
        startActivity(intent);
    }

    /**
     * Result from async
     *
     * @param result
     */
    public void ReturnThreadResult(String result)
    {
        if (result.contains("Авторизация пройдена"))
        {
            // получаем данные пользователя
            UsersManager.get(editEmail.getText().toString(), this);
        }

        // результат содержит json результат с данными
        if (result.contains("result"))
        {
            User user = UsersManager.extractUserFromJson(result);

            if (user != null)
            {
                // регистрируем операцию в базе
                Operation loginOperation = new Operation(user.getName() + " авторизовался",
                                                         "",
                                                         -1,
                                                         -1,
                                                         user.getId_user(),
                                                         -1);
                OperationsManager.add(loginOperation);

                MainActivity.user = user;

                Toast toast = Toast.makeText(getApplicationContext(),
                        "Добро пожаловать!", Toast.LENGTH_SHORT);
                toast.show();

                Intent intent = new Intent(LoginActivity.this, MainActivity.class);
                startActivity(intent);
            }
            else
            {
                Toast toast = Toast.makeText(getApplicationContext(),
                        "Ошибка", Toast.LENGTH_SHORT);
                toast.show();
            }
        }

        if (result.contains("Некорректные данные")) {
            Toast toast = Toast.makeText(getApplicationContext(),
                    "Некорректные данные", Toast.LENGTH_SHORT);
            toast.show();
        }

        if (result.equals("Ошибка")) {
            Toast toast = Toast.makeText(getApplicationContext(),
                    "Ошибка", Toast.LENGTH_SHORT);
            toast.show();
        }
    }
}
