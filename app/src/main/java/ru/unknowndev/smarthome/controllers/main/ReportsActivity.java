package ru.unknowndev.smarthome.controllers.main;

import android.os.Bundle;
import android.support.v7.app.AppCompatActivity;
import android.support.v7.widget.Toolbar;
import android.view.Menu;
import android.view.MenuItem;
import android.view.View;
import android.widget.ArrayAdapter;
import android.widget.EditText;
import android.widget.ListView;
import android.widget.Toast;

import java.util.List;

import ru.unknowndev.smarthome.R;
import ru.unknowndev.smarthome.models.Operation;
import ru.unknowndev.smarthome.services.OperationsManager;
import ru.unknowndev.smarthome.services.Settings;

public class ReportsActivity extends AppCompatActivity {

    protected ListView listview;
    protected EditText dateFromEdit, dateToEdit;
    protected Toolbar mActionBarToolbar;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_reports);

        listview = (ListView) findViewById(R.id.operations_list_view);
        dateFromEdit = (EditText)findViewById(R.id.date_from_edit);
        dateToEdit = (EditText)findViewById(R.id.date_to_edit);

        // объявляем бар
        mActionBarToolbar = (Toolbar) findViewById(R.id.toolbar_actionbar);
        mActionBarToolbar.setTitle("");
        // задаем настроенный экшн бар
        setSupportActionBar(mActionBarToolbar);
        // добавляем к бару кнопку назад
        if (getSupportActionBar() != null){
            getSupportActionBar().setDisplayHomeAsUpEnabled(true);
            getSupportActionBar().setDisplayShowHomeEnabled(true);
        }
    }

    @Override
    public boolean onCreateOptionsMenu(Menu menu) {
        getMenuInflater().inflate(R.menu.back_menu, menu);
        return true;
    }

    @Override
    public boolean onOptionsItemSelected(MenuItem item) {
        if (item.getItemId() == android.R.id.home) {
            finish();
        }
        return super.onOptionsItemSelected(item);
    }

    public void applyClick(View view)
    {
        // прячем клавиатуру
        Settings.hideKeyboard(this);

        String dateFrom = dateFromEdit.getText().toString();
        String dateTo = dateToEdit.getText().toString();
        OperationsManager.getOperations(dateFrom, dateTo, this);
    }

    public void ReturnThreadResult(String s) {

        if (s.contains("result_operations")) {
            List<Operation> operations = OperationsManager.extractOperationsFromJSON(s);
            // задаем массив слов из названий устройств
            //String[] names = OperationsManager.generateNames(operations);
            String[] names = new String[operations.size()];

            int i = 0;
            for (Operation operation : operations ) {
                names[i] = "Дата: " + operation.getDate() + ". " + operation.getName();
                i++;
            }

            // создаем адаптер
            ArrayAdapter<String> adapter = new ArrayAdapter<String>(this,
                    android.R.layout.simple_list_item_1, names);

            listview.setAdapter(adapter);

            Toast toast = Toast.makeText(getApplicationContext(),
                    "Отчет сформирован", Toast.LENGTH_SHORT);
            toast.show();
        }
    }
}
