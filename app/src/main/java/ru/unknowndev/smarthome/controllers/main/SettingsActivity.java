package ru.unknowndev.smarthome.controllers.main;

import android.content.SharedPreferences;
import android.os.Bundle;
import android.support.v7.app.AppCompatActivity;
import android.support.v7.widget.Toolbar;
import android.view.Menu;
import android.view.MenuItem;
import android.view.View;
import android.widget.EditText;
import android.widget.Toast;

import ru.unknowndev.smarthome.R;
import ru.unknowndev.smarthome.controllers.MainActivity;
import ru.unknowndev.smarthome.services.Settings;

public class SettingsActivity extends AppCompatActivity {

    // поля ввода
    private EditText editDBAdress, editArduinoAdress;
    protected Toolbar mActionBarToolbar;
    // объект с настройками
    private SharedPreferences sp;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_settings);

        // получаем поля
        editDBAdress = (EditText)findViewById(R.id.adress_database_edit);
        editArduinoAdress = (EditText)findViewById(R.id.adress_arduino_edit);
        // получаем поля

        // объект с настройками
        sp = MainActivity.settings_app;
        // задаем полю сохраненные настройки
        editDBAdress.setText(Settings.getDatabaseAdress(sp));
        // задаем полю сохраненные настройки
        editArduinoAdress.setText(Settings.getArduinoAdress(sp));

        // объявляем бар
        mActionBarToolbar = (Toolbar) findViewById(R.id.toolbar_actionbar);
        mActionBarToolbar.setTitle("");
        // задаем настроенный экшн бар
        setSupportActionBar(mActionBarToolbar);
        // добавляем к бару кнопку назад
        if (getSupportActionBar() != null){
            getSupportActionBar().setDisplayHomeAsUpEnabled(true);
            getSupportActionBar().setDisplayShowHomeEnabled(true);
        }
    }

    @Override
    public boolean onCreateOptionsMenu(Menu menu) {
        getMenuInflater().inflate(R.menu.back_menu, menu);
        return true;
    }

    @Override
    public boolean onOptionsItemSelected(MenuItem item) {
        if (item.getItemId() == android.R.id.home) {
            finish();
        }
        return super.onOptionsItemSelected(item);
    }

    public void saveClick(View view)
    {
        // прячем клавиатуру
        Settings.hideKeyboard(this);

        String adressDB = editDBAdress.getText().toString();
        String adressArduino = editArduinoAdress.getText().toString();

        Settings.setDatabaseAdress(sp, adressDB);
        Settings.setArduinoAdress(sp, adressArduino);

        // обновляем в главных настройках объект с настройками
        MainActivity.settings_app = sp;

        Toast toast = Toast.makeText(getApplicationContext(),
                "Настройки сохранены", Toast.LENGTH_SHORT);
        toast.show();
    }
}
