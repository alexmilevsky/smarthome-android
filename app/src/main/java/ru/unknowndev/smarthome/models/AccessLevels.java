package ru.unknowndev.smarthome.models;

/**
 * Данные прав доступа
 */

public class AccessLevels {
    protected int id_access_level;
    protected String name;
    protected String description;

    public AccessLevels(int id_access_level, String name, String description) {
        this.id_access_level = id_access_level;
        this.name = name;
        this.description = description;
    }
}
