package ru.unknowndev.smarthome.models;

/**
 * Данные подмодуля
 */

public class SubModule {
    protected int id_sub_module;
    protected String name;
    protected String description;

    public SubModule(int id_sub_module, String name, String description) {
        this.id_sub_module = id_sub_module;
        this.name = name;
        this.description = description;
    }
}
