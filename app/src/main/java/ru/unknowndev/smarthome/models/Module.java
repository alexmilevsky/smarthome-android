package ru.unknowndev.smarthome.models;

import ru.unknowndev.smarthome.controllers.MainActivity;

/**
 * Данные модуля
 */

public class Module {
    protected int id_module;
    protected String name;
    protected String description;
    protected String status;
    protected int id_sub_module;

    public Module(int id_module, String name, String description, String status, int id_sub_module) {
        this.id_module = id_module;
        this.name = name;
        this.description = description;
        this.status = status;
        this.id_sub_module = id_sub_module;
    }

    public int getId_module() {
        return id_module;
    }

    public void setId_module(int id_module) {
        this.id_module = id_module;
    }

    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }

    public String getDescription() {
        return description;
    }

    public void setDescription(String description) {
        this.description = description;
    }

    public String getStatus() {
        return status;
    }

    public void setStatus(String status) {
        this.status = status;
    }

    public int getId_sub_module() {
        return id_sub_module;
    }

    public void setId_sub_module(int id_sub_module) {
        this.id_sub_module = id_sub_module;
    }

    /**
     * Возвращает индекс модуля в списке
     *
     * @return
     */
    public int getIndexInList()
    {
        int index = 0;
        for (Module currentModule : MainActivity.modules) {
            if (currentModule.getId_module() == this.getId_module()) {
                index = MainActivity.modules.indexOf(currentModule);
                break;
            }
        }
        return index;
    }
}
