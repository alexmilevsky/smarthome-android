package ru.unknowndev.smarthome.models;

/**
 * Данные пользователя
 */

public class User {
    protected int id_user;
    protected String email;
    protected String password;
    protected String surname;
    protected String name;
    protected String patronymic;
    protected String birthday;
    protected String gender;
    protected String phone;
    protected int id_group;

    public User()
    {

    }

    public User(int id_user, String email, String password, String surname, String name, String patronymic, String birthday, String gender, String phone, int id_group) {
        this.id_user = id_user;
        this.email = email;
        this.password = password;
        this.surname = surname;
        this.name = name;
        this.patronymic = patronymic;
        this.birthday = birthday;
        this.gender = gender;
        this.phone = phone;
        this.id_group = id_group;
    }

    public User(String email, String password, String surname, String name, String patronymic, String birthday, String gender, String phone, int id_group) {
        this.email = email;
        this.password = password;
        this.surname = surname;
        this.name = name;
        this.patronymic = patronymic;
        this.birthday = birthday;
        this.gender = gender;
        this.phone = phone;
        this.id_group = id_group;
    }

    public User(int id_user, String email, String password, String surname, String name, String patronymic, String birthday, String gender, String phone, AccessLevels group) {
        this.id_user = id_user;
        this.email = email;
        this.password = password;
        this.surname = surname;
        this.name = name;
        this.patronymic = patronymic;
        this.birthday = birthday;
        this.gender = gender;
        this.phone = phone;
        this.id_group = group.id_access_level;
    }

    public int getId_user() {
        return id_user;
    }

    public void setId_user(int id_user) {
        this.id_user = id_user;
    }

    public String getEmail() {
        return email;
    }

    public void setEmail(String email) {
        this.email = email;
    }

    public String getPassword() {
        return password;
    }

    public void setPassword(String password) {
        this.password = password;
    }

    public String getSurname() {
        return surname;
    }

    public void setSurname(String surname) {
        this.surname = surname;
    }

    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }

    public String getPatronymic() {
        return patronymic;
    }

    public void setPatronymic(String patronymic) {
        this.patronymic = patronymic;
    }

    public String getBirthday() {
        return birthday;
    }

    public void setBirthday(String birthday) {
        this.birthday = birthday;
    }

    public String getGender() {
        return gender;
    }

    public void setGender(String gender) {
        this.gender = gender;
    }

    public String getPhone() {
        return phone;
    }

    public void setPhone(String phone) {
        this.phone = phone;
    }

    public int getId_group() {
        return id_group;
    }

    public void setId_group(int id_group) {
        this.id_group = id_group;
    }
}