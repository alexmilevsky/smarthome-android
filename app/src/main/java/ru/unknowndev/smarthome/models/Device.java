package ru.unknowndev.smarthome.models;

import ru.unknowndev.smarthome.controllers.MainActivity;

/**
 * Данные об устройстве
 */

public class Device {
    protected int id_device;
    protected String name;
    protected String description;
    protected String status;
    protected int pin;
    protected String value;
    protected int id_device_type;
    protected int id_module;

    public Device (int id_device, String name, String description, String status, int pin, String value, DevicesType type, Module module) {
        this.id_device = id_device;
        this.name = name;
        this.description = description;
        this.status = status;
        this.pin = pin;
        this.value = value;
        this.id_device_type = type.id_device_type;
        this.id_module = module.id_module;
    }

    public Device (int id_device, String name, String description, String status, int pin, String value, int id_device_type, int id_module) {
        this.id_device = id_device;
        this.name = name;
        this.description = description;
        this.status = status;
        this.pin = pin;
        this.value = value;
        this.id_device_type = id_device_type;
        this.id_module = id_module;
    }

    public Device (String name, String description, String status, int pin, String value, int id_device_type, int id_module) {
        this.name = name;
        this.description = description;
        this.status = status;
        this.pin = pin;
        this.value = value;
        this.id_device_type = id_device_type;
        this.id_module = id_module;
    }

    @Override
    public String toString() {
        return name + ". ID: " + id_device + " Pin: " + pin;
    }

    public int getId_device() {
        return id_device;
    }

    public void setId_device(int id_device) { this.id_device = id_device; }

    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }

    public String getDescription() {
        return description;
    }

    public void setDescription(String description) {
        this.description = description;
    }

    public String getStatus() {
        return status;
    }

    public void setStatus(String status) {
        this.status = status;
    }

    public int getPin() {
        return pin;
    }

    public void setPin(int pin) {
        this.pin = pin;
    }

    public String getValue() {
        return value;
    }

    public void setValue(String value) {
        this.value = value;
    }

    public int getId_device_type() {
        return id_device_type;
    }

    public void setId_device_type(int id_device_type) {
        this.id_device_type = id_device_type;
    }

    public int getId_module() {
        return id_module;
    }

    public void setId_module(int id_module) {
        this.id_module = id_module;
    }

    /**
     * Возвращает индекс устройства в списке
     *
     * @return
     */
    public int getIndexInList()
    {
        int index = 0;
        for (Device currentDevice : MainActivity.devices) {
            if (currentDevice.getId_device() == this.getId_device()) {
                index = MainActivity.devices.indexOf(currentDevice);
                break;
            }
        }
        return index;
    }
}
