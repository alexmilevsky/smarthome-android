package ru.unknowndev.smarthome.models;

/**
 * Тип устройства
 */

public class DevicesType {
    protected int id_device_type;
    protected String name;
    protected String description;

    public DevicesType(int id_device_type, String name, String description) {
        this.id_device_type = id_device_type;
        this.name = name;
        this.description = description;
    }

    public int getId_device_type() {
        return id_device_type;
    }

    public void setId_device_type(int id_device_type) {
        this.id_device_type = id_device_type;
    }

    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }

    public String getDescription() {
        return description;
    }

    public void setDescription(String description) {
        this.description = description;
    }
}
