package ru.unknowndev.smarthome.models;

/**
 * Данные операции
 */

public class Operation {
    protected int id_operation;
    protected String name;
    protected String description;
    protected String date;
    protected int id_module;
    protected int id_user;
    protected int id_device;
    protected int id_home;

    public Operation(int id_operation, String name, String description, String date, int id_module, int id_device, int id_user, int id_home) {
        this.id_operation = id_operation;
        this.name = name;
        this.description = description;
        this.date = date;
        this.id_module = id_module;
        this.id_device = id_device;
        this.id_user = id_user;
        this.id_home = id_home;
    }

    public Operation(String name, String description, int id_module, int id_device, int id_user, int id_home) {
        this.name = name;
        this.description = description;
        this.id_module = id_module;
        this.id_device = id_device;
        this.id_user = id_user;
        this.id_home = id_home;
    }

    public int getId_operation() {
        return id_operation;
    }

    public void setId_operation(int id_operation) {
        this.id_operation = id_operation;
    }

    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }

    public String getDescription() {
        return description;
    }

    public void setDescription(String description) {
        this.description = description;
    }

    public int getId_module() {
        return id_module;
    }

    public void setId_module(int id_module) {
        this.id_module = id_module;
    }

    public int getId_user() {
        return id_user;
    }

    public void setId_user(int id_user) {
        this.id_user = id_user;
    }

    public int getId_device() {
        return id_device;
    }

    public void setId_device(int id_device) {
        this.id_device = id_device;
    }

    public int getId_home() {
        return id_home;
    }

    public void setId_home(int id_home) {
        this.id_home = id_home;
    }

    public String getDate() {
        return date;
    }

    public void setDate(String date) {
        this.date = date;
    }
}
