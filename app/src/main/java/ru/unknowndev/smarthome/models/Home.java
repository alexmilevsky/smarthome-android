package ru.unknowndev.smarthome.models;

/**
 * Данные о доме
 */

public class Home {
    protected int id_home;
    protected String access_key;
    protected String name;
    protected String status;

    public Home(int id_home, String access_key, String name, String status) {
        this.id_home = id_home;
        this.access_key = access_key;
        this.name = name;
        this.status = status;
    }
}