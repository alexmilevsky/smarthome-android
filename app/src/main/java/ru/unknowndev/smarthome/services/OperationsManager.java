package ru.unknowndev.smarthome.services;

import android.app.ProgressDialog;
import android.os.AsyncTask;

import org.json.JSONArray;
import org.json.JSONException;
import org.json.JSONObject;

import java.text.SimpleDateFormat;
import java.util.ArrayList;
import java.util.Calendar;
import java.util.HashMap;
import java.util.List;

import ru.unknowndev.smarthome.controllers.MainActivity;
import ru.unknowndev.smarthome.controllers.main.ReportsActivity;
import ru.unknowndev.smarthome.models.Operation;

public class OperationsManager {

    /**
     * Добавляет операцию в базу, без ответа о результате
     *
     * @param operation
     */
    static public void add(final Operation operation)
    {
        class AddOperation extends AsyncTask<Void,Void,String> {
            @Override
            protected void onPreExecute() {
                super.onPreExecute();
            }
            @Override
            protected void onPostExecute(String s) {
                super.onPostExecute(s);
            }
            @Override
            protected String doInBackground(Void... v) {
                HashMap<String,String> params = new HashMap<>();

                Calendar c = Calendar.getInstance();
                SimpleDateFormat df = new SimpleDateFormat("yyyy-MM-dd HH:mm:ss");
                String formattedDate = df.format(c.getTime());

                // передаем данные
                params.put("name",operation.getName());
                params.put("description",operation.getDescription());
                params.put("date",formattedDate);
                params.put("id_module",Integer.toString(operation.getId_module()));
                params.put("id_user",Integer.toString(operation.getId_user()));
                params.put("id_device",Integer.toString(operation.getId_device()));
                params.put("id_home",Integer.toString(operation.getId_home()));

                // адрес базы
                String adressDB = Settings.getDatabaseAdress(MainActivity.settings_app);

                RequestHandler rh = new RequestHandler();
                String res = rh.sendPostRequest(adressDB + "add_operation.php", params);
                return res;
            }
        }
        AddOperation ao = new AddOperation();
        ao.execute();
    }

    static public void getOperations(final String fromDate, final String toDate, final ReportsActivity parent)
    {
        class GetOperations extends AsyncTask<Void,Void,String> {
            ProgressDialog loading;
            @Override
            protected void onPreExecute() {
                super.onPreExecute();
                loading = ProgressDialog.show(parent,"Загрузка...","Подождите...",false,false);
            }

            @Override
            protected void onPostExecute(String s) {
                super.onPostExecute(s);
                loading.dismiss();
                parent.ReturnThreadResult(s);
            }
            @Override
            protected String doInBackground(Void... v) {

                HashMap<String,String> params = new HashMap<>();

                // передаем данные
                params.put("datefrom",fromDate);
                params.put("dateto",toDate);

                // адрес базы
                String adressDB = Settings.getDatabaseAdress(MainActivity.settings_app);

                RequestHandler c = new RequestHandler();
                String s = c.sendPostRequest(adressDB + "get_all_operations.php", params);
                return s;
            }
        }
        GetOperations go = new GetOperations();
        go.execute();
    }

    public static List<Operation> extractOperationsFromJSON(String json)
    {
        List<Operation> operationsList = new ArrayList<Operation>();
        JSONObject jsonObject = null;
        try {
            jsonObject = new JSONObject(json);
            JSONArray result = jsonObject.getJSONArray("result_operations");

            for(int i = 0; i<result.length(); i++){
                JSONObject jo = result.getJSONObject(i);

                Operation operation = new Operation(jo.getInt("id_operation"),
                        jo.getString("name"),
                        jo.getString("description"),
                        jo.getString("date"),
                        jo.getInt("id_module"),
                        jo.getInt("id_device"),
                        jo.getInt("id_user"),
                        jo.getInt("id_home")
                );

                operationsList.add(operation);
            }

        } catch (JSONException e) {
            e.printStackTrace();
        }
        return operationsList;
    }
}
