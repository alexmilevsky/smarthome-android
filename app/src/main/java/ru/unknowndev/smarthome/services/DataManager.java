package ru.unknowndev.smarthome.services;

import android.app.ProgressDialog;
import android.os.AsyncTask;

import ru.unknowndev.smarthome.controllers.MainActivity;

/**
 * Created by unknown on 02.04.2017.
 */

public class DataManager {
    /**
     * get all devices
     */
    static public void getAll(final MainActivity parent)
    {
        class GetAll extends AsyncTask<Void,Void,String> {
            ProgressDialog loading;
            @Override
            protected void onPreExecute() {
                super.onPreExecute();
                loading = ProgressDialog.show(parent,"Загрузка...","Подождите...",false,false);
            }

            @Override
            protected void onPostExecute(String s) {
                super.onPostExecute(s);
                loading.dismiss();
                parent.ReturnThreadResult(s);
            }
            @Override
            protected String doInBackground(Void... params) {

                // адрес базы
                String adressDB = Settings.getDatabaseAdress(MainActivity.settings_app);

                RequestHandler c = new RequestHandler();
                String s = c.sendGetRequest(adressDB + "get_all_data.php");
                return s;
            }
        }
        GetAll ga = new GetAll();
        ga.execute();
    }
}
