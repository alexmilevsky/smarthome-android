package ru.unknowndev.smarthome.services;

import android.app.Activity;
import android.content.Context;
import android.content.SharedPreferences;
import android.view.inputmethod.InputMethodManager;


/**
 * Class for all settings with program
 */
public class Settings {

    // файл для настроек
    public static final String APP_PREFERENCES = "settings";
    public static final String APP_PREFERENCES_DB_ADRESS = "Database_adress";
    public static final String APP_PREFERENCES_ARDUINO_ADRESS = "Arduino_adress";

    /**
     * Возвращает объект с настройками
     *
     * @param ctxt
     * @return
     */
    public static SharedPreferences getSharedPreferences (Context ctxt) {
        return ctxt.getSharedPreferences(APP_PREFERENCES, 0);
    }

    /**
     * Получает адрес базы данных из объекта с настройками
     *
     * @param sp
     * @return
     */
    public static String getDatabaseAdress(SharedPreferences sp) {
        return sp.getString(APP_PREFERENCES_DB_ADRESS, "http://127.0.0.1/");
    }

    /**
     * Получает адрес ардуино из объекта с настройками
     *
     * @param sp
     * @return
     */
    public static String getArduinoAdress(SharedPreferences sp) {
        return sp.getString(APP_PREFERENCES_ARDUINO_ADRESS, "http://127.0.0.1/");
    }

    /**
     * Задает адрес базы данных из объекта с настройками
     *
     * @param sp
     * @param adress
     */
    public static void setDatabaseAdress(SharedPreferences sp, String adress)
    {
        SharedPreferences.Editor editor = sp.edit();
        editor.putString(APP_PREFERENCES_DB_ADRESS, adress);
        editor.apply();
    }

    /**
     * Задает адрес базы данных из объекта с настройками
     *
     * @param sp
     * @param adress
     */
    public static void setArduinoAdress(SharedPreferences sp, String adress)
    {
        SharedPreferences.Editor editor = sp.edit();
        editor.putString(APP_PREFERENCES_ARDUINO_ADRESS, adress);
        editor.apply();
    }

    /**
     * Hide keyboard
     *
     * @param activity
     */
    public static void hideKeyboard(Activity activity)
    {
        InputMethodManager inputMethodManager =
                (InputMethodManager) activity.getSystemService(
                        Activity.INPUT_METHOD_SERVICE);
        inputMethodManager.hideSoftInputFromWindow(
                activity.getCurrentFocus().getWindowToken(), 0);
    }
}
