package ru.unknowndev.smarthome.services;

import android.app.ProgressDialog;
import android.os.AsyncTask;

import org.json.JSONArray;
import org.json.JSONException;
import org.json.JSONObject;

import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;

import ru.unknowndev.smarthome.controllers.device.DevicesActivity;
import ru.unknowndev.smarthome.controllers.MainActivity;
import ru.unknowndev.smarthome.controllers.device.AddDeviceActivity;
import ru.unknowndev.smarthome.controllers.device.ControlDeviceActivity;
import ru.unknowndev.smarthome.models.Device;
import ru.unknowndev.smarthome.models.DevicesType;

public class DevicesManager {

    /**
     * add device into database
     *
     * @param device
     * @param parent output
     */
    static public void add(final Device device, final AddDeviceActivity parent)
    {
        class AddDevice extends AsyncTask<Void,Void,String> {
            ProgressDialog loading;
            @Override
            protected void onPreExecute() {
                super.onPreExecute();
                loading = ProgressDialog.show(parent,"Загрузка...","Подождите...",false,false);
            }
            @Override
            protected void onPostExecute(String s) {
                super.onPostExecute(s);
                loading.dismiss();
                parent.ReturnThreadResult(s);
            }
            @Override
            protected String doInBackground(Void... v) {
                HashMap<String,String> params = new HashMap<>();

                // передаем данные
                params.put("name",device.getName());
                params.put("description",device.getDescription());
                params.put("status",device.getStatus());
                params.put("pin",Integer.toString(device.getPin()));
                params.put("value",device.getValue());
                params.put("id_device_type",Integer.toString(device.getId_device_type()));
                params.put("id_module",Integer.toString(device.getId_module()));

                // адрес базы
                String adressDB = Settings.getDatabaseAdress(MainActivity.settings_app);

                RequestHandler rh = new RequestHandler();
                String res = rh.sendPostRequest(adressDB + "add_device.php", params);
                return res;
            }
        }
        AddDevice ad = new AddDevice();
        ad.execute();
    }

    /**
     * update device in database
     *
     * @param device
     * @param parent output
     */
    static public void update(final Device device, final ControlDeviceActivity parent)
    {
        class UpdateDevice extends AsyncTask<Void,Void,String> {
            ProgressDialog loading;
            @Override
            protected void onPreExecute() {
                super.onPreExecute();
                loading = ProgressDialog.show(parent,"Загрузка...","Подождите...",false,false);
            }
            @Override
            protected void onPostExecute(String s) {
                super.onPostExecute(s);
                loading.dismiss();
                parent.ReturnThreadResult(s);
            }
            @Override
            protected String doInBackground(Void... v) {
                HashMap<String,String> params = new HashMap<>();

                // передаем данные
                params.put("id_device",Integer.toString(device.getId_device()));
                params.put("name",device.getName());
                params.put("description",device.getDescription());
                params.put("status",device.getStatus());
                params.put("pin",Integer.toString(device.getPin()));
                params.put("value",device.getValue());
                params.put("id_device_type",Integer.toString(device.getId_device_type()));
                params.put("id_module",Integer.toString(device.getId_module()));

                // адрес базы
                String adressDB = Settings.getDatabaseAdress(MainActivity.settings_app);

                RequestHandler rh = new RequestHandler();
                String res = rh.sendPostRequest(adressDB + "update_device.php", params);
                return res;
            }
        }
        UpdateDevice ud = new UpdateDevice();
        ud.execute();
    }

    /**
     * remove device from database
     *
     * @param device
     * @param parent output
     */
    static public void remove(final Device device, final ControlDeviceActivity parent)
    {
        class RemoveDevice extends AsyncTask<Void,Void,String> {
            ProgressDialog loading;
            @Override
            protected void onPreExecute() {
                super.onPreExecute();
                loading = ProgressDialog.show(parent,"Загрузка...","Подождите...",false,false);
            }
            @Override
            protected void onPostExecute(String s) {
                super.onPostExecute(s);
                loading.dismiss();
                parent.ReturnThreadResult(s);
            }
            @Override
            protected String doInBackground(Void... v) {
                HashMap<String,String> params = new HashMap<>();

                // передаем данные
                params.put("id_device",Integer.toString(device.getId_device()));

                // адрес базы
                String adressDB = Settings.getDatabaseAdress(MainActivity.settings_app);

                RequestHandler rh = new RequestHandler();
                String res = rh.sendPostRequest(adressDB + "remove_device.php", params);
                return res;
            }
        }
        RemoveDevice rd = new RemoveDevice();
        rd.execute();
    }

    /**
     * get all devices
     */
    static public void getDevices(final DevicesActivity parent)
    {
        class GetDevices extends AsyncTask<Void,Void,String> {
            ProgressDialog loading;
            @Override
            protected void onPreExecute() {
                super.onPreExecute();
                loading = ProgressDialog.show(parent,"Загрузка...","Подождите...",false,false);
            }

            @Override
            protected void onPostExecute(String s) {
                super.onPostExecute(s);
                loading.dismiss();
                parent.ReturnThreadResult(s);
            }
            @Override
            protected String doInBackground(Void... params) {

                // адрес базы
                String adressDB = Settings.getDatabaseAdress(MainActivity.settings_app);

                RequestHandler c = new RequestHandler();
                String s = c.sendGetRequest(adressDB + "get_all_devices.php");
                return s;
            }
        }
        GetDevices gd = new GetDevices();
        gd.execute();
    }

    /**
     * get all devices of module
     */
    static public void getDevices(final int id_module, final DevicesActivity parent)
    {
        class GetDevices extends AsyncTask<Void,Void,String> {
            ProgressDialog loading;
            @Override
            protected void onPreExecute() {
                super.onPreExecute();
                loading = ProgressDialog.show(parent,"Загрузка...","Подождите...",false,false);
            }

            @Override
            protected void onPostExecute(String s) {
                super.onPostExecute(s);
                loading.dismiss();
                parent.ReturnThreadResult(s);
            }
            @Override
            protected String doInBackground(Void... params) {

                // адрес базы
                String adressDB = Settings.getDatabaseAdress(MainActivity.settings_app);

                RequestHandler c = new RequestHandler();
                String s = c.sendGetRequestParam(adressDB + "get_all_devices.php?id_module=",Integer.toString(id_module));
                return s;
            }
        }
        GetDevices gd = new GetDevices();
        gd.execute();
    }

    public static List<Device> extractDevicesFromJSON(String json)
    {
        List<Device> devicesList = new ArrayList<Device>();
        JSONObject jsonObject = null;
        try {
            jsonObject = new JSONObject(json);
            JSONArray result = jsonObject.getJSONArray("result_devices");

            for(int i = 0; i<result.length(); i++){
                JSONObject jo = result.getJSONObject(i);

                Device device = new Device(jo.getInt("id_device"),
                                           jo.getString("name"),
                                           jo.getString("description"),
                                           jo.getString("status"),
                                           jo.getInt("pin"),
                                           jo.getString("value"),
                                           jo.getInt("id_device_type"),
                                           jo.getInt("id_module")
                );

                devicesList.add(device);
            }

        } catch (JSONException e) {
            e.printStackTrace();
        }
        return devicesList;
    }

    public static List<DevicesType> extractDevicesTypeFromJSON(String json)
    {
        List<DevicesType> devicesTypeList = new ArrayList<DevicesType>();
        JSONObject jsonObject = null;
        try {
            jsonObject = new JSONObject(json);
            JSONArray result = jsonObject.getJSONArray("result_devices_type");

            for(int i = 0; i<result.length(); i++){
                JSONObject jo = result.getJSONObject(i);

                DevicesType deviceType = new DevicesType(jo.getInt("id_device_type"),
                        jo.getString("name"),
                        jo.getString("description")
                );

                devicesTypeList.add(deviceType);
            }

        } catch (JSONException e) {
            e.printStackTrace();
        }
        return devicesTypeList;
    }

    /**
     * Подсчитывает количество устройств в модуле
     *
     * @param id_module
     * @return
     */
    public static int countDevicesInModule(int id_module)
    {
        int countDeviceOfModule = 0;
        // находим количество устройств у модуля
        for (Device device : MainActivity.devices) {
            if (device.getId_module() == id_module) countDeviceOfModule++;
        }
        return countDeviceOfModule;
    }

    /**
     * Генерирует массив имен для устройств модуля
     *
     * @param count
     * @param id_module
     * @return
     */
    public static String[] generateNamesForDevices(int count, int id_module)
    {
        String[] names = new String[count];
        int i = 0;
        for (Device device : MainActivity.devices) {
            if (device.getId_module() == id_module) {
                names[i] = device.toString();
                i++;
            }
        }
        return names;
    }

    /**
     * Получает устройство из списка по id
     *
     * @param id
     * @return
     */
    public static Device getDeviceById(int id)
    {
        Device device = null;
        for (Device currentDevice : MainActivity.devices) {
            if (currentDevice.getId_device() == id) {
                device = currentDevice;
                break;
            }
        }
        return device;
    }

    /**
     * Устанавливает значение на устройстве
     *
     * @param device
     * @param parent
     */
    public static void setValueOnDevice(final Device device, final ControlDeviceActivity parent)
    {
        class SetValueOfDevice extends AsyncTask<Void,Void,String> {
            ProgressDialog loading;
            @Override
            protected void onPreExecute() {
                super.onPreExecute();
                loading = ProgressDialog.show(parent,"Загрузка...","Подождите...",false,false);
            }
            @Override
            protected void onPostExecute(String s) {
                super.onPostExecute(s);
                loading.dismiss();
                parent.ReturnThreadResult(s);
            }
            @Override
            protected String doInBackground(Void... v) {
                HashMap<String,String> params = new HashMap<>();

                // передаем данные
                String port = Integer.toString(device.getPin());
                String value = device.getValue();
                String status = device.getStatus();

                // адрес базы
                String adressArduino = Settings.getArduinoAdress(MainActivity.settings_app);

                RequestHandler rh = new RequestHandler();
                String res = rh.sendGetRequest(adressArduino + "?PORT=" + port + "&VALUE=" + value + "&STATUS=" + status);
                return res;
            }
        }
        SetValueOfDevice sv = new SetValueOfDevice();
        sv.execute();
    }

}