package ru.unknowndev.smarthome.services;

import android.app.ProgressDialog;
import android.os.AsyncTask;

import org.json.JSONArray;
import org.json.JSONException;
import org.json.JSONObject;

import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;

import ru.unknowndev.smarthome.controllers.device.DevicesActivity;
import ru.unknowndev.smarthome.controllers.MainActivity;
import ru.unknowndev.smarthome.models.Module;
import ru.unknowndev.smarthome.models.SubModule;

public class ModulesManager {

    /**
     * Get data of all modules
     *
     * @param parent
     */
    static public void getModules(final MainActivity parent)
    {
        class GetModules extends AsyncTask<Void,Void,String> {
            ProgressDialog loading;
            @Override
            protected void onPreExecute() {
                super.onPreExecute();
                loading = ProgressDialog.show(parent,"Загрузка...","Подождите...",false,false);
            }

            @Override
            protected void onPostExecute(String s) {
                super.onPostExecute(s);
                loading.dismiss();
                parent.ReturnThreadResult(s);
            }
            @Override
            protected String doInBackground(Void... params) {

                // адрес базы
                String adressDB = Settings.getDatabaseAdress(MainActivity.settings_app);

                RequestHandler c = new RequestHandler();
                String s = c.sendGetRequest(adressDB + "get_all_modules.php");
                return s;
            }
        }
        GetModules gm = new GetModules();
        gm.execute();
    }

    static public void update(final Module module, final DevicesActivity parent)
    {
        class UpdateModule extends AsyncTask<Void,Void,String> {
            ProgressDialog loading;
            @Override
            protected void onPreExecute() {
                super.onPreExecute();
                loading = ProgressDialog.show(parent,"Загрузка...","Подождите...",false,false);
            }
            @Override
            protected void onPostExecute(String s) {
                super.onPostExecute(s);
                loading.dismiss();
                parent.ReturnThreadResult(s);
            }
            @Override
            protected String doInBackground(Void... v) {
                HashMap<String,String> params = new HashMap<>();

                // передаем данные
                params.put("id_module",Integer.toString(module.getId_module()));
                params.put("name",module.getName());
                params.put("description",module.getDescription());
                params.put("status",module.getStatus());
                params.put("id_sub_module",Integer.toString(module.getId_sub_module()));

                // адрес базы
                String adressDB = Settings.getDatabaseAdress(MainActivity.settings_app);

                RequestHandler rh = new RequestHandler();
                String res = rh.sendPostRequest(adressDB + "update_module.php", params);
                return res;
            }
        }
        UpdateModule um = new UpdateModule();
        um.execute();
    }

    public static List<Module> extractModulesFromJSON(String json)
    {
        List<Module> modulesList = new ArrayList<Module>();
        JSONObject jsonObject = null;
        try {
            jsonObject = new JSONObject(json);
            JSONArray result = jsonObject.getJSONArray("result_modules");

            for(int i = 0; i<result.length(); i++){
                JSONObject jo = result.getJSONObject(i);

                Module module = new Module(jo.getInt("id_module"),
                        jo.getString("name"),
                        jo.getString("description"),
                        jo.getString("status"),
                        jo.getInt("id_sub_module")
                );

                modulesList.add(module);
            }

        } catch (JSONException e) {
            e.printStackTrace();
        }
        return modulesList;
    }

    public static List<SubModule> extractModulesTypesFromJSON(String json)
    {
        List<SubModule> subModulesList = new ArrayList<SubModule>();
        JSONObject jsonObject = null;
        try {
            jsonObject = new JSONObject(json);
            JSONArray result = jsonObject.getJSONArray("result_modules");

            for(int i = 0; i<result.length(); i++){
                JSONObject jo = result.getJSONObject(i);

                SubModule subModule = new SubModule(jo.getInt("id_sub_module"),
                        jo.getString("name"),
                        jo.getString("description")
                );

                subModulesList.add(subModule);
            }

        } catch (JSONException e) {
            e.printStackTrace();
        }
        return subModulesList;
    }

    /**
     * Получает модуль из списка по id
     *
     * @param id
     * @return
     */
    public static Module getModuleById(int id)
    {
        Module module = null;
        for (Module currentModule : MainActivity.modules) {
            if (currentModule.getId_module() == id) {
                module = currentModule;
                break;
            }
        }
        return module;
    }

    /**
     * Получает модуль из списка по имени
     *
     * @param name
     * @return
     */
    public static Module getModuleByName(String name)
    {
        Module module = null;
        for (Module currentModule : MainActivity.modules) {
            if (currentModule.getName().equals(name)) {
                module = currentModule;
                break;
            }
        }
        return module;
    }
}
