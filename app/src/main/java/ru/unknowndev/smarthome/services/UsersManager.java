package ru.unknowndev.smarthome.services;

import android.app.ProgressDialog;
import android.os.AsyncTask;

import org.json.JSONArray;
import org.json.JSONException;
import org.json.JSONObject;

import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;

import ru.unknowndev.smarthome.controllers.MainActivity;
import ru.unknowndev.smarthome.controllers.user.LoginActivity;
import ru.unknowndev.smarthome.controllers.user.ProfileActivity;
import ru.unknowndev.smarthome.controllers.user.RegistrationActivity;
import ru.unknowndev.smarthome.models.AccessLevels;
import ru.unknowndev.smarthome.models.User;

public class UsersManager {

    /**
     * add user into database
     *
     * @param user
     * @param parent output
     */
    static public void add(final User user, final RegistrationActivity parent)
    {
        class AddUser extends AsyncTask<Void,Void,String> {
            ProgressDialog loading;
            @Override
            protected void onPreExecute() {
                super.onPreExecute();
                loading = ProgressDialog.show(parent,"Загрузка...","Подождите...",false,false);
            }
            @Override
            protected void onPostExecute(String s) {
                super.onPostExecute(s);
                loading.dismiss();
                parent.ReturnThreadResult(s);
            }
            @Override
            protected String doInBackground(Void... v) {
                HashMap<String,String> params = new HashMap<>();
                // передаем данные
                params.put("email",user.getEmail());
                params.put("password",user.getPassword());
                if (!user.getSurname().equals("")) params.put("surname",user.getSurname());
                if (!user.getName().equals("")) params.put("name",user.getName());
                if (!user.getPatronymic().equals("")) params.put("patronymic",user.getPatronymic());
                if (!user.getBirthday().equals("")) params.put("birthday",user.getBirthday());
                if (!user.getGender().equals("")) params.put("gender",user.getGender());
                if (!user.getPhone().equals("")) params.put("phone",user.getPhone());
                // задаем группу пользователя
                params.put("id_access_level",String.valueOf(user.getId_group()));
                // передаем данные

                // адрес базы
                String adressDB = Settings.getDatabaseAdress(MainActivity.settings_app);

                RequestHandler rh = new RequestHandler();
                String res = rh.sendPostRequest(adressDB + "add_user.php", params);
                return res;
            }
        }
        // почта и пароль не пусты
        if (!user.getEmail().equals("") && !user.getPassword().equals("")) {
            // сначала проверить нету ли такого пользователя

            // тогда создаем нового
            AddUser au = new AddUser();
            au.execute();
        }
    }

    /**
     * get user
     *
     * @param email
     * @param parent
     */
    static public void get(final String email, final LoginActivity parent)
    {
        class GetUser extends AsyncTask<Void,Void,String> {
            ProgressDialog loading;
            @Override
            protected void onPreExecute() {
                super.onPreExecute();
                loading = ProgressDialog.show(parent,"Загрузка...","Подождите...",false,false);
            }

            @Override
            protected void onPostExecute(String s) {
                super.onPostExecute(s);
                loading.dismiss();
                parent.ReturnThreadResult(s);
            }
            @Override
            protected String doInBackground(Void... params) {

                // адрес базы
                String adressDB = Settings.getDatabaseAdress(MainActivity.settings_app);

                RequestHandler c = new RequestHandler();
                String s = c.sendGetRequestParam(adressDB + "get_user.php?email=",email);
                return s;
            }
        }
        GetUser gu = new GetUser();
        gu.execute();
    }

    /**
     * update user in database
     *
     * @param user
     * @param parent output
     */
    static public void update(final User user, final ProfileActivity parent)
    {
        class UpdateUser extends AsyncTask<Void,Void,String> {
            ProgressDialog loading;
            @Override
            protected void onPreExecute() {
                super.onPreExecute();
                loading = ProgressDialog.show(parent,"Загрузка...","Подождите...",false,false);
            }
            @Override
            protected void onPostExecute(String s) {
                super.onPostExecute(s);
                loading.dismiss();
                parent.ReturnThreadResult(s);
            }
            @Override
            protected String doInBackground(Void... v) {
                HashMap<String,String> params = new HashMap<>();

                params.put("email",user.getEmail());
                params.put("password",user.getPassword());
                params.put("surname",user.getSurname());
                params.put("name",user.getName());
                params.put("patronymic",user.getPatronymic());
                params.put("birthday",user.getBirthday());
                params.put("gender",user.getGender());
                params.put("phone",user.getPhone());
                params.put("id_access_level",String.valueOf(user.getId_group()));

                // адрес базы
                String adressDB = Settings.getDatabaseAdress(MainActivity.settings_app);

                RequestHandler rh = new RequestHandler();
                String res = rh.sendPostRequest(adressDB + "update_user.php", params);
                return res;
            }
        }
        UpdateUser uu = new UpdateUser();
        uu.execute();
    }

    /**
     * authorizate user from db
     *
     * @param email
     * @param password
     * @param parent
     */
    static public void authorizate(final String email, final String password, final String keyHome, final LoginActivity parent)
    {
        class AuthorizateUser extends AsyncTask<Void,Void,String> {
            ProgressDialog loading;
            @Override
            protected void onPreExecute() {
                super.onPreExecute();
                loading = ProgressDialog.show(parent,"Загрузка...","Подождите...",false,false);
            }

            @Override
            protected void onPostExecute(String s) {
                super.onPostExecute(s);
                loading.dismiss();
                parent.ReturnThreadResult(s);
            }
            @Override
            protected String doInBackground(Void... params) {

                // адрес базы
                String adressDB = Settings.getDatabaseAdress(MainActivity.settings_app);

                RequestHandler c = new RequestHandler();
                String s = c.sendGetRequest(adressDB + "authorizate_user.php?email=" + email + "&password=" + password + "&keyhome=" + keyHome);
                return s;
            }
        }
        AuthorizateUser au = new AuthorizateUser();
        au.execute();
    }

    /**
     * Извлекает объект пользователя из json
     *
     * @param json
     * @return
     */
    public static User extractUserFromJson(String json)
    {
        User user = null;
        try {
            JSONObject jsonObject = new JSONObject(json);
            JSONArray result = jsonObject.getJSONArray("result");
            JSONObject c = result.getJSONObject(0);

            user = new User(Integer.parseInt(c.getString("id_user")),
                                             c.getString("email"),
                                             c.getString("password"),
                                             c.getString("surname"),
                                             c.getString("name"),
                                             c.getString("patronymic"),
                                             c.getString("birthday"),
                                             c.getString("gender"),
                                             c.getString("phone"),
                                             Integer.parseInt(c.getString("id_access_level"))
                                );
        } catch (JSONException e) {
            user = null;
        }
        return user;
    }

    public static List<AccessLevels> extractAccessLevelsFromJSON(String json)
    {
        List<AccessLevels> accessLevelsList = new ArrayList<AccessLevels>();
        JSONObject jsonObject = null;
        try {
            jsonObject = new JSONObject(json);
            JSONArray result = jsonObject.getJSONArray("result_access_levels");

            for(int i = 0; i<result.length(); i++){
                JSONObject jo = result.getJSONObject(i);

                AccessLevels subModule = new AccessLevels(jo.getInt("id_access_level"),
                        jo.getString("name"),
                        jo.getString("description")
                );

                accessLevelsList.add(subModule);
            }

        } catch (JSONException e) {
            e.printStackTrace();
        }
        return accessLevelsList;
    }
}
